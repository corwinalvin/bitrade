var current_prices;
if (typeof is_frame == 'undefined' || !is_frame) {
    // if (!location.hostname.match(/bitcoin\.co\.id/gi)) {
    //     window.location = 'https://vip.bitcoin.co.id/';
    // }
    if (window.top !== window.self) top.location = self.location;
}
$(function() {
    if (typeof is_idr_market !== 'undefined' && is_idr_market) {} else if (typeof is_btc_market !== 'undefined' && is_btc_market) {} else {
        up_to_date(true);
    }
    $('#spot-graph').on('show.bs.dropdown', function() {
        $('.spot_ajax_wait').html('<img src="' + ROOT + 'v2/images/loaders/loader8.gif" />');
        open_spot_stats();
    })
    $('#notification-box').on('show.bs.dropdown', function() {
        $('.notification_box_wait').html('<div class="text-center padding10"><img src="' + ROOT + 'v2/images/loaders/loader8.gif" /></div>');
        $('#notification_count').addClass('hide');
        $.ajax({
            url: ROOT + 'notifications/content',
            dataType: 'json'
        }).done(function(data) {
            $('.notification_box_wait').html(data.html);
        });
    });
    var open_spot_stats = function() {
        $.ajax({
            url: API_BASE + 'api/btc_idr/hourly_chart_data',
            dataType: 'json'
        }).done(function(data) {
            var hargaBtc = data;
            var maxHargaBtc = 0;
            var minHargaBtc = 1000000000000;
            jQuery.each(hargaBtc, function(index, value) {
                var harga = value[1];
                if (harga > maxHargaBtc) maxHargaBtc = harga;
                if (harga < minHargaBtc) minHargaBtc = harga;
            });
            var plot = jQuery.plot(jQuery("#spotflot"), [{
                data: hargaBtc,
                color: "#428BCA",
                label: '1 BTC'
            }], {
                tooltip: {
                    show: true,
                    content: "%x, 1 BTC = %y"
                },
                series: {
                    label: 'Bitcoin Spot Market Price',
                    lines: {
                        show: true,
                        fill: false,
                        lineWidth: 1,
                        fillColor: {
                            colors: [{
                                opacity: 0.5
                            }]
                        }
                    },
                    points: {
                        show: false
                    },
                    shadowSize: 1
                },
                legend: {
                    position: 'nw'
                },
                grid: {
                    hoverable: true,
                    clickable: false,
                    borderColor: '#ddd',
                    borderWidth: 1,
                    labelMargin: 10,
                    backgroundColor: '#fff'
                },
                yaxis: {
                    show: true,
                    color: '#fff',
                    min: Math.round(minHargaBtc * 0.995),
                    max: Math.round(maxHargaBtc * 1.005)
                },
                xaxis: {
                    show: true,
                    color: '#fff',
                    mode: "time",
                    tickSize: [3, "hour"],
                    timezone: "browser"
                }
            });
            var previousPoint = null;
            jQuery("#basicflot").bind("plothover", function(event, pos, item) {
                jQuery("#x").text(pos.x.toFixed(2));
                jQuery("#y").text(pos.y.toFixed(2));
                if (item) {
                    if (previousPoint != item.dataIndex) {
                        previousPoint = item.dataIndex;
                        jQuery("#tooltip").remove();
                        var x = item.datapoint[0],
                            y = item.datapoint[1];
                        showTooltip(item.pageX, item.pageY, new Date(x * 1000).format('h:i') + "<br />" + rp_format(y));
                    }
                } else {
                    jQuery("#tooltip").remove();
                    previousPoint = null;
                }
            });
        });
    }

    function showTooltip(x, y, contents) {
        jQuery('<div id="tooltip" class="tooltipflot">' + contents + '</div>').css({
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 5
        }).appendTo("body").fadeIn(200);
    }
    jQuery('select option:first-child').text('');
    jQuery("select").select2({
        minimumResultsForSearch: -1
    });
});
var thousep_regex = new RegExp(thousep.replace('.', '\\.'), 'g');

function update_balance(data) {
    if (data.hasOwnProperty('balance')) {
        for (var k in data.balance) {
            var o = data.balance[k];
            if (k == 'rp') {
                balance_rp = data.balance.rp * 1;
                $('.balance_rp_val').html($.number(balance_rp, 0, decpoint, thousep));
            } else if (k == 'frozen_rp') {
                balance_frozen_rp = data.balance.frozen_rp * 1;
                $('.balance_frozen_rp_val').html($.number(balance_frozen_rp, 0, decpoint, thousep));
            } else {
                eval('balance_' + k + ' = data.balance.' + k + '/1e8');
                $('.balance_' + k + '_val').html(btc_format(data.balance[k] / 1e8));
            }
        }
        $('.total_assets_val').html($.number(data.asset_value, 0, decpoint, thousep));
        if (balance_rp > 0) $('.deposit_idr_link').addClass('hide');
        else $('.deposit_idr_link').removeClass('hide');
        if (balance_btc > 0) $('.deposit_btc_link').addClass('hide');
        else $('.deposit_btc_link').removeClass('hide');
        if (data.notification_count > 0) {
            $('#notification_count').html(data.notification_count < 100 ? data.notification_count : '99+').show();
        } else {
            $('#notification_count').html('0').hide();
        }
    }
}

function update_price_all(prices) {
    current_prices = prices;
    $.each(prices, function(key, value) {
        var change = '';
        var value = value * 1;
        var before = 0;
        var before_html = $('.price_' + key + '_val').html();
        if (before_html == undefined) return;
        if (key.substr(-3) == 'btc') {
            if (before_html.substr(0, 1).match(/[0-9]/)) before = $('.price_' + key + '_val').html().replace(',', '.');
            value = value / 1e8;
            eval('price_' + key + '=value');
            eval('coin_price_round=' + key.replace(/btc$/, '') + '_price_round');
            $('.price_' + key + '_val').html(parseFloat(value).toFixed(coin_price_round).replace(thousep, decpoint));
        } else {
            if (before_html.substr(0, 1).match(/[0-9]/)) before = $('.price_' + key + '_val').html().replace(/[^0-9]/g, '');
            eval('price_' + key + '=value');
            $('.price_' + key + '_val').html($.number(value, 0, decpoint, thousep));
        }
        if ($('.price_' + key + '_val').parents('.market-row').hasClass('active'))
            var background_before = '#428BCA';
        else if ($('.price_' + key + '_val').parents('.market-row').hasClass('odd'))
            var background_before = '#f9f9f9';
        else
            var background_before = 'none';
        if (!before) return;
        else if (before > value) {
            $('.price_' + key + '_val').parents('.market-row').children('td').css('background-color', '#FF6377').animate({
                backgroundColor: background_before
            }, 1000);
            $('.price_' + key + '_val').parents('.market-price-box').css('background-color', '#FF6377').animate({
                backgroundColor: ''
            }, 1000);
        } else if (before < value) {
            $('.price_' + key + '_val').parents('.market-row').children('td').css('background-color', '#4BEF61').animate({
                backgroundColor: background_before
            }, 1000);
            $('.price_' + key + '_val').parents('.market-price-box').css('background-color', '#4BEF61').animate({
                backgroundColor: ''
            }, 1000);
        }
    });
}

function update_volumes(volumes) {
    if (!volumes) return;
    var vol_string;
    var hover_string;
    $.each(volumes, function(key, value) {
        if (key.substr(-3) == 'btc') {
            vol_string = parseFloat(value.btc).toFixed(1).replace(thousep, decpoint) + ' BTC';
            $('.vol_' + key + '_val').html(vol_string);
            $('.vol_' + key + '_val').attr('title', vol_string);
        } else {
            if (value.idr > 1000000000000) vol_string = $.number(parseFloat(value.idr / 1000000000000), 1, decpoint, thousep) + 'tn IDR';
            else if (value.idr > 1000000000) vol_string = $.number(parseFloat(value.idr / 1000000000), 1, decpoint, thousep) + 'bn IDR';
            else if (value.idr > 1000000) vol_string = $.number(parseFloat(value.idr / 1000000), 1, decpoint, thousep) + 'mn IDR';
            else if (value.idr > 1000) vol_string = $.number(parseFloat(value.idr / 1000), 1, decpoint, thousep) + 'k IDR';
            else vol_string = $.number(parseFloat(value.idr), 0, decpoint, thousep) + ' IDR';
            hover_string = $.number(parseFloat(value.idr), 0, decpoint, thousep) + ' IDR';
            $('.vol_' + key + '_val').html(vol_string);
            $('.vol_' + key + '_val').attr('title', hover_string);
        }
    });
}

function update_price_arrow(prices, prices_24h) {
    $.each(prices, function(key, value) {
        var class_name = '.price_change_' + key;
        $(class_name).removeClass('arrow-green').removeClass('arrow-red');
        var change = Math.round((prices[key] - prices_24h[key]) / prices_24h[key] * 1000) / 10;
        if (!prices_24h[key]) $(class_name).css({
            color: '#636e7b'
        }).html('-');
        else if (change > 0) $(class_name).css({
            color: '#12b600'
        }).addClass('arrow-green').html('&nbsp;<i class="fa fa-chevron-circle-up"></i> ' + change + '%');
        else if (change == 0) $(class_name).css({
            color: '#636e7b'
        }).html(change + '%');
        else $(class_name).css({
            color: '#E6393E'
        }).addClass('arrow-red').html('&nbsp;<i class="fa fa-chevron-circle-down"></i> ' + Math.abs(change) + '%');
    });
}
var up_to_date = function(first_load) {
    var count_complete = 0;
    $('.reload-spot-data i').addClass('fa-spin');
    $.ajax({
        url: API_BASE + 'api/btc_idr/webdata',
        dataType: 'json',
        data: {
            lang: LANG
        },
        type: 'post'
    }).done(function(data) {
        update_price_all(data.prices);
        update_volumes(data.volumes);
        update_price_arrow(data.prices, data.prices_24h);
        last_price = data.last_price;
        high_price = data._24h.high;
        low_price = data._24h.low;
        vol_btc = data._24h.vol_btc / 1e8;
        vol_rp = data._24h.vol_rp;
        var buy_orders = data.buy_orders;
        var sell_orders = data.sell_orders;
        bid_price = data.buy_orders[0].price;
        ask_price = data.sell_orders[0].price;
        if (typeof page_id !== 'undefined' && (page_id == 'btcidr' || page_id == 'dashboard')) {
            var old_title = $('title').html();
            var new_title = '(' + rp_format(last_price) + ' IDR/BTC) ' + old_title.replace(/\(.+\)/, '');
            $('title').html(new_title);
        }
        $('.last_price_val').html($.number(data.prices.btcidr, 0, decpoint, thousep));
        $('.low_val').html($.number(data._24h.low, 0, decpoint, thousep));
        $('.high_val').html($.number(data._24h.high, 0, decpoint, thousep));
        $('.vol_val').html($.number(vol_btc, vol_btc > 1 ? 1 : 2, decpoint, thousep)).attr('title', rp_format(vol_rp) + ' IDR');
        $('#sell_orders tbody').html(data.frame_sell_orders);
        $('#buy_orders tbody').html(data.frame_buy_orders);
        $('#last_trades tbody').html(data.frame_last_trades);
        if ($('.js-marquee').html()) $('.js-marquee').html(data.marquee);
        else $('.marquee').html(data.marquee);
        if (first_load) {
            $('#buy_price').val((ask_price * 1) - 100).change();
            $('#sell_price').val((bid_price * 1) + 100).change();
        }
        // $('.order_table tbody tr td').click(function() {
        //     var price = $(this).parent('tr').attr('price');
        //     $('#buy_price, #sell_price').val(price).change();
        // });
        // if (typeof updateChart == 'function' && !first_load) {
        //     updateChart();
        // }
    }).always(function() {
        count_complete++;
        if (count_complete >= 2) $('.reload-spot-data i').removeClass('fa-spin');
    });
    // if (typeof symbol === 'undefined') symbol = 'BTCIDR';
    // $.ajax({
    //     url: ROOT + 'market_data/' + symbol,
    //     dataType: 'json',
    //     type: 'post',
    //     data: {
    //         ds: ds
    //     }
    // }).done(function(data) {
    //     if (data.need_login) {
    //         window.location = window.location;
    //     }
    //     update_balance(data);
    //     if (symbol) {
    //         $('#pending_orders_container').html(data.frame_pending_orders);
    //         $('#my_last_trades tbody').html(data.frame_my_last_trades);
    //     }
    //     if (data.notification_count > 0) {
    //         $('#notification_count').html(data.notification_count < 100 ? data.notification_count : '99+').removeClass('hide');
    //     } else {
    //         $('#notification_count').html('0').addClass('hide');
    //     }
    // }).always(function() {
    //     count_complete++;
    //     if (count_complete >= 2) $('.reload-spot-data i').removeClass('fa-spin');
    // });
}
// buy_estimation = function() {
//     var rp = ($('#buy_amount_rp').val() + '').replace(/[^0-9]/g, '');
//     var price = ($('#buy_price').val() + '').replace(/[^0-9]/g, '');
//     if (rp < trade_rp_min || price < 1) {
//         $('#buy_fee_calc, #buy_btc_calc').html('-');
//         return;
//     }
//     var buy_fee = Math.floor(rp * trade_fee);
//     var rp_remain = rp - buy_fee;
//     var btc = (Math.floor(rp_remain / price * 100000000) / 100000000) + '';
//     var btc_without_fee = (Math.floor(rp / price * 100000000) / 100000000) + '';
//     $('#buy_fee_calc').html(rp_format(buy_fee) + ' IDR');
//     $('#buy_btc_calc').html(btc_format(btc_without_fee) + ' BTC');
// }
// $(function() {
//     $('#buy_amount_rp, #buy_price').keyup(function() {
//         buy_estimation();
//     });
//     $('#buy_amount_rp, #buy_price').change(function() {
//         buy_estimation();
//     });
// });
// sell_estimation = function() {
//     var btc = ($('#sell_amount_btc').val() + '').replace(/,/g, '.') * 1;
//     var price = ($('#sell_price').val() + '').replace(/[^0-9]/g, '');
//     if (btc < trade_btc_min || price < 1) {
//         $('#sell_fee_calc, #sell_btc_calc').html('-');
//         return;
//     }
//     var rp = btc * price;
//     var sell_fee = Math.floor(rp * trade_fee);
//     var rp_remain = rp - sell_fee;
//     $('#sell_fee_calc').html(rp_format(sell_fee) + ' IDR');
//     $('#sell_btc_calc').html(rp_format(rp_remain) + ' IDR');
// }
// $(function() {
//     $('#sell_amount_btc, #sell_price').keyup(function() {
//         sell_estimation();
//     });
//     $('#sell_amount_btc, #sell_price').change(function() {
//         sell_estimation();
//     });
// });

// function rtrim(str, charlist) {
//     charlist = !charlist ? ' \\s\u00A0' : (charlist + '').replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '\\$1');
//     var re = new RegExp('[' + charlist + ']+$', 'g');
//     return (str + '').replace(re, '');
// }
// jQuery('select option:first-child').text('');
// $(function() {
//     var btn_sms = jQuery('.btn-send-sms-pin');
//     var btn_voice = jQuery('.btn-send-voice-pin');
//     var btn_sms_text = jQuery('.sms_desc_text');
//     var btn_voice_text = jQuery('.voice_desc_text');
//     var btn_action_error = false;
//     btn_sms.click(function() {
//         btn_action_error = false;
//         $('.sms-pin').val('');
//         btn_sms.fadeOut().addClass('hide');
//         btn_sms.after('<img src="' + ROOT + 'v2/images/loaders/loader8.gif" class="btn-send-sms-pin-loading" />');
//         var delay = 30;
//         btn_sms.prepend(' <span id="countdown"></span> ');
//         btn_sms.prop('disabled', true);
//         btn_voice.prop('disabled', true);
//         btn_voice.fadeOut().addClass('hide');
//         $.ajax({
//             url: ROOT + 'ajax/send_sms',
//             dataType: 'json',
//             type: 'post'
//         }).done(function(data) {
//             if (data.is_error) {
//                 btn_action_error = true;
//                 alert_error(data.error);
//                 return;
//             }
//             alert_notify(data.msg);
//         }).fail(function() {
//             btn_action_error = true;
//             jalert("Server tidak merespon. Mohon cek koneksi internet anda.\nServer not responding. Please check your internet connection.");
//         }).always(function() {
//             btn_sms.removeClass('hide').fadeIn();
//             $('.btn-send-sms-pin-loading').remove();
//             if (!btn_action_error) {
//                 var downloadTimer = setInterval(function() {
//                     $("#countdown").html('(' + delay + ') ');
//                     if (delay <= 0) clearInterval(downloadTimer);
//                     delay--;
//                 }, 1000);
//                 setTimeout(function() {
//                     btn_sms.find('span#countdown').remove();
//                     btn_sms.prop('disabled', false);
//                     btn_voice.prop('disabled', false);
//                     btn_voice.removeClass('hide').fadeIn();
//                     btn_sms_text.addClass('hide');
//                     btn_voice_text.removeClass('hide').fadeIn();
//                 }, delay * 1000);
//             } else {
//                 btn_sms.prop('disabled', false);
//                 btn_voice.prop('disabled', false);
//             }
//         });
//         return false;
//     });
//     btn_voice.click(function() {
//         btn_action_error = 0;
//         $('.sms-pin').val('');
//         btn_voice.fadeOut().addClass('hide');
//         btn_voice.after('<img src="' + ROOT + 'v2/images/loaders/loader8.gif" class="btn-send-sms-pin-loading" />');
//         var delay = 60;
//         btn_voice.prepend(' <span id="countdown"></span> ');
//         btn_voice.prop('disabled', true);
//         btn_sms.prop('disabled', true);
//         btn_sms.fadeOut().addClass('hide');
//         $.ajax({
//             url: ROOT + 'ajax/send_voice',
//             dataType: 'json',
//             type: 'post'
//         }).done(function(data) {
//             if (data.is_error) {
//                 btn_action_error = 1;
//                 alert_error(data.error);
//                 return;
//             }
//             alert_notify(data.msg);
//         }).fail(function() {
//             btn_action_error = 1;
//             jalert("Server tidak merespon. Mohon cek koneksi internet anda.\nServer not responding. Please check your internet connection.");
//         }).always(function() {
//             btn_voice.removeClass('hide').fadeIn();
//             $('.btn-send-sms-pin-loading').remove();
//             if (!btn_action_error) {
//                 var downloadTimer = setInterval(function() {
//                     $("#countdown").html('(' + delay + ') ');
//                     if (delay <= 0) clearInterval(downloadTimer);
//                     delay--;
//                 }, 1000);
//                 setTimeout(function() {
//                     btn_voice.find('span#countdown').remove();
//                     btn_voice.prop('disabled', false);
//                     btn_sms.prop('disabled', false);
//                     btn_sms.removeClass('hide').fadeIn();
//                 }, delay * 1000);
//             } else {
//                 btn_sms.prop('disabled', false);
//                 btn_voice.prop('disabled', false);
//             }
//         });
//         return false;
//     });
// });