var alert_success=function(text,url){if(url)var redirect='window.location=\''+url+'\';';else var redirect='window.location=window.location;';var alert_html='<div class="modal modal-alert-success" tabindex="-1" role="dialog"  data-backdrop="static">\
            <div class="modal-dialog modal-md">\
              <div class="modal-content">\
				  <div class="panel panel-info">\
					  <div class="panel-body">'+text+'</div>\
					  <div class="panel-footer padding10">\
						<div style="text-align:right">\
							<button class="btn btn-primary" onclick="'+redirect+'"><i class="fa fa-times"></i> Close</button>\
						</div>\
					  </div>\
					</div>\
              </div>\
            </div>\
        </div>';$(alert_html).modal('show');}
var alert_error=function(text){var alert_html='<div class="modal modal-alert-success" tabindex="-1" role="dialog"  data-backdrop="static">\
            <div class="modal-dialog modal-md">\
              <div class="modal-content">\
				  <div class="panel panel-danger-alt">\
					  <div class="panel-heading">Error</div>\
					  <div class="panel-body">'+text+'</div>\
					  <div class="panel-footer padding10">\
						<div style="text-align:right">\
							<button class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>\
						</div>\
					  </div>\
					</div>\
              </div>\
            </div>\
        </div>';$(alert_html).modal('show');}
var alert_notify=function(text){var alert_html='<div class="modal modal-alert-notify" tabindex="-1" role="dialog"  data-backdrop="static">\
            <div class="modal-dialog modal-md">\
              <div class="modal-content">\
				  <div class="panel panel-info">\
					  <div class="panel-body">'+text+'</div>\
					  <div class="panel-footer padding10">\
						<div style="text-align:right">\
							<button class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>\
						</div>\
					  </div>\
					</div>\
              </div>\
            </div>\
        </div>';$(alert_html).modal('show');}
var calert=alert_notify;