<?php
return [
	'name' => 'Nama',
	'enter-name' => 'Masukkan nama Anda',
	'password' => 'Kata Sandi',
	'enter-password' => 'Masukkan Password Anda',
	'remember' => 'Ingatkan saya',
	'forgot' => 'Lupa Kata Sandi',
	'login' => 'Masuk'
];