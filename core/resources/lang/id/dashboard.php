<?php
return [
	'follow' => 'Ikuti kami di',
	'payment-method' => 'Metode pembayaran yang kami terima',
	'get-support' => 'Bantuan di',
	'register' => 'Registrasi',
	'login' => 'Masuk',
	'account' => 'Akun',
	'logout' => 'Keluar',
	'dashboard' => 'Dasbor',
	'home' => 'Beranda',
	'about' => 'Tentang',
	'faq' => 'FAQ',
	'contact' => 'Kontak',
	'days-online' => 'Hidup Online',
	'new-open-ticket' => 'Buat Tiket Baru'
];