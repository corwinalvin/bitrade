<?php
return [
	'name' => 'Name',
	'enter-name' => 'Enter your name',
	'password' => 'Password',
	'enter-password' => 'Enter your password',
	'remember' => 'Remember me',
	'forgot' => 'Forgot Password',
	'login' => 'Login'
];