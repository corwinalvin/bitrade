<?php
return [
	'follow' => 'Follow us on',
	'payment-method' => 'Payment Method that we accept',
	'get-support' => 'Get support on',
	'register' => 'Register',
	'login' => 'Login',
	'account' => 'Account',
	'logout' => 'Logout',
	'dashboard' => 'Dashboard',
	'home' => 'Home',
	'about' => 'About',
	'faq' => 'FAQ',
	'contact' => 'Contact',
	'days-online' => 'Days Online',
	'new-open-ticket' => 'Open New Ticket Support'
];