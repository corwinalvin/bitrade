
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{{ $site_title }} | {{ $page_title }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/png" href="{{ asset('assets/images/favicon.png') }}">
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/lain.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/slicknav.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/asRange.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.2/css/AdminLTE.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.2/css/skins/_all-skins.min.css">
    <link href="{{ asset('assets/css/color.php?color='.$basic->color) }}" rel="stylesheet">
    <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/admin/css/components-rounded.min.css')}}" rel="stylesheet" id="style_components" type="text/css"/>
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
    <script src="{{ asset('assets/js/jquery.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{asset('assets/js/wizard.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/alert.js')}}" type="text/javascript"></script>
    <script src="https://indodax.com/v2/js/jquery.number.js"></script>
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
    <script src="{{ asset('assets/js/jquery-ui.min.js') }}" type="text/javascript"></script>
    <script src="https://indodax.com/v2/js/flot/jquery.flot.min.js"></script>
    <script src="https://indodax.com/v2/js/flot/jquery.flot.resize.min.js"></script>
    <script src="https://indodax.com/v2/js/flot/jquery.flot.spline.min.js"></script>
    <script src="https://indodax.com/v2/js/flot/jquery.flot.time.min.js"></script>
    <script src="https://indodax.com/v2/js/flot/jquery.flot.tooltip.min.js"></script>
    <script src="{{asset('assets/js/jquery.ajax-cross-origin.min.js')}}" type="text/javascript"></script>
    <!-- <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet"> -->
    @yield('css')
    <script type="text/javascript">
            var balance_rp = {{$balance}};
            var THOUSEP = '.';
            var DECPOINT = ',';
            var ROOT = 'https://indodax.com/';
            var API_BASE = 'https://api2.bitcoin.co.id/';
            var last_price = 0;
            var high_price = 0;
            var low_price = 0;
            var vol_btc = 0;
            var vol_rp = 0;
            var trade_btc_min = 0.0001;
            var trade_rp_min = 1000;
            var trade_fee = 0.3/100;
            var thousep = '.';
            var decpoint = ',';
            var LANG = 'indonesia';
            var gauth_enabled = false;
            var bid_price = 0;
            var ask_price = 0;
            var TIMEZONE = 7;
            var bts_price_round = 8;
            var drk_price_round = 6;
            var doge_price_round = 8;
            var eth_price_round = 5;
            var ltc_price_round = 6;
            var nxt_price_round = 8;
            var str_price_round = 8;
            var nem_price_round = 8;
            var xrp_price_round = 8;
    </script>
    <script src="https://indodax.com/v2/js/number_format.js?20180215"></script>
    <script src="{{asset('assets/js/main-core.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/jquery.idle.min.js')}}" type="text/javascript"></script>

</head>
<body class="skin-yellow-light sidebar-mini box-layout" data-widget="tree">
@if (!Auth::guest())
    
    <div class="wrapper">
        <!-- START @LOADING ANIMATION -->   
        <div class="splash">
            <div class="color-line"></div>
            <div class="splash-title">          
                <img alt="..." src="{{url('assets/images/Preloader_8.gif')}}" />
                <p>loading...</p>
            </div>
        </div>
        <!-- Main Header -->
        <header class="main-header" id="header">
        
        <a href="#" class="logo">
            <span class="logo-mini">
                <b>BT</b>
            </span>
            <!-- Logo -->
            <span href="#" class="logo-lg">
                <img src="{{ asset('assets/images/logo.png') }}" class="img img-thumbnail" align="left" style="background-color: transparent!important;border-color: transparent;width: 70%;margin-top:8px;">
            </span>
        </a>

            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li id="spot-graph" class="dropdown btn-group btn-group-list">
                            <button type="button" class="dropdown-toggle btn btn-warning" data-toggle="dropdown" aria-expanded="true">
                                <span class="btc_stats">1 BTC = Rp
                                    <span class="price_btcidr_val"><img src="https://indodax.com/v2/images/loaders/loader8.gif" /></span>
                                </span>
                            </button>
                            <div class="dropdown-menu pull-right">
                                <h5>Bitcoin Spot Market Stats</h5>
                                <div class="tinystat padding10 btc-stats-detail" style="display: none;">
                                    <div class="datainfo col-md-3">
                                        <span class="text-muted">Last Price</span>
                                        <h4 class="last_price_val spot_ajax_wait">148.872.000</h4>
                                    </div>
                                    <div class="datainfo col-md-3">
                                        <span class="text-muted">Low</span>
                                        <h4 class="low_val spot_ajax_wait">136.498.000</h4>
                                    </div>
                                    <div class="datainfo col-md-3">
                                        <span class="text-muted">High</span>
                                        <h4 class="high_val spot_ajax_wait">149.800.000</h4>
                                    </div>
                                    <div class="datainfo col-md-3">
                                        <span class="text-muted">Vol 24h</span>
                                        <h4 class="vol_val spot_ajax_wait" title="85.777.704.940 IDR">595,7</h4>
                                    </div>
                                </div>
                                <div class="height200 text-center flot-graph-loading flotGraph spot_ajax_wait" id="spotflot" style="padding: 0px;position: relative;"></div>
                                <div class="dropdown-footer text-center">
                                    <a href="{{route('market')}}">Buka Bitcoin Spot Market</a>
                                </div>
                            </div>
                        </li>
                        <li class="dropdown tasks-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                @if(session('active_lang') == "id")
                                Indonesia @else English @endif
                            </a>
                            <ul class="dropdown-menu">
                                @foreach (Config::get('languages') as $lang => $language)
                                <li>
                                    <a href="{{route('lang.switch', $lang)}}" class="nav-link">
                                        <span class="title">{{$language}}</span>
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                        <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="hidden-xs">Menu</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{!! route('edit-profile') !!}">@lang('user-dashboard.edit-profile') </a></li>
                                <li><a href="{!! route('change-password') !!}">@lang('user-dashboard.change-password')</a></li>
                                <li class="nav-item">
                                    <a href="{!! route('support-all') !!}" class="nav-link nav-toggle">
                                        <span class="title">@lang('user-dashboard.get-support')</span></a>
                                </li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">@lang('user-dashboard.logout')
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- Left side column. contains the logo and sidebar -->
        @include('layouts.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="min-height: 100% !important;">
            @yield('content')
        </div>

        <!-- Main Footer -->
        <footer class="main-footer" style="max-height: 100px;text-align: left">
            <div class="pull-right hidden-xs">
                <b>Version</b>
                1.0
            </div>
            <strong>Copyright © {{date('Y')}} <a href="#">{{config('app.name')}}</a>.</strong> 
        </footer>

    </div>
@else
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{!! url('/') !!}">
                    InfyOm Generator
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{!! url('/home') !!}">Home</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    <li><a href="{!! url('/login') !!}">Login</a></li>
                    <li><a href="{!! url('/register') !!}">Register</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    @endif

    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{{ asset('assets/admin/js/clipboard.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/owl.carousel.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/jquery.slicknav.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/jquery-asRange.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/main.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/admin/js/sweetalert.min.js') }}" type="text/javascript"></script>
    <script>
        function setTooltip(t,i){$(t).tooltip("hide").attr("data-original-title",i).tooltip("show")}function hideTooltip(t){setTimeout(function(){$(t).tooltip("hide")},1e3)}$(".has").tooltip({trigger:"click",placement:"bottom"}),$(document).ready(function(){$(document).on("click",".delete_button",function(t){var i=$(this).data("id");$(".abir_id").val(i)})}),$("#btnYes").click(function(){$("#formSubmit").submit()});
        !function(){"use strict";document.body.addEventListener("click",function(e){var t=e.target,c=t.dataset.copytarget,o=c?document.querySelector(c):null;if(o&&o.select){o.select();try{document.execCommand("copy"),o.blur(),t.classList.add("copied"),setTimeout(function(){t.classList.remove("copied")},1500)}catch(e){alert("please press Ctrl/Cmd+C to copy")}}},!0)}();
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.2/js/adminlte.min.js"></script>
    <script src="{{ asset('assets/admin/js/jquery.waypoints.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/admin/js/bootstrap-tooltip.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/admin/js/jquery.counterup.min.js') }}" type="text/javascript"></script>

    @yield('scripts')
    <script>
        ;(function($) {         
            $(window).bind("load", function () {            
                $('.splash').css('display', 'none');
            });
        })(jQuery);

  </script>
</body>
</html>