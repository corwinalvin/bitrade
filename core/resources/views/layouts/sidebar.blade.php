<aside class="main-sidebar" id="sidebar-wrapper">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar" style="height: auto;">
		<div class="user-panel">
			<div class="pull-left image">
				<img src="https://adminlte.io/themes/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle">
			</div>
			<div class="pull-left info">
				<p>{!! Auth::user()->name !!}</p>
				<small class="grey">{!!Auth::user()->username!!}</small>
			</div>
		</div>
		<ul class="sidebar-menu tree">
			<li class="header" style="color:#303030">MENU UTAMA</li>
			@include('layouts.menu')
		</ul>
		<!-- /.sidebar-menu -->
	</section>
	<!-- /.sidebar -->
</aside>