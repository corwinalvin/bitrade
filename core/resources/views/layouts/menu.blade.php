<li class="{{ Request::is('user/dashboard') ? 'active' : '' }}">
	<a href="{!! route('user-dashboard') !!}"><i class="ion ion-speedometer"></i> <span>@lang('user-dashboard.dashboard')</span></a>
</li>
<li class="{{ Request::is('user/market*') ? 'active' : '' }}">
	<a href="{!! route('market') !!}"><i class="fa fa-line-chart"></i> <span>@lang('user-dashboard.spot-market')</span></a>
</li>
<li class="{{ Request::is('user/finance*') ? 'active' : '' }}">
	<a href="{!! route('finance') !!}"><i class="fa fa-bitcoin"></i> <span>@lang('user-dashboard.finance')</span></a>
</li>
@if (!Auth::user()->google2fa_secret)
<li class="{{ Request::is('user/enable_gauth') ? 'active' : '' }}">
	<a href="{!! route('enable_gauth') !!}"><i class="fa fa-google"></i> <span>Google Authenticator</span></a>
</li>
@endif
@if(Auth::User()->identity_verify == 0)
<li class="{{ Request::is('user/verification') ? 'active' : '' }}">
	<a href="{!! route('verification') !!}"><i class="ion ion-ios-person"></i> <span>@lang('user-dashboard.verify-account')</span></a>
</li>
@endif
<li class="treeview {{ Request::is('user/investment*') || Request::is('user/user-repeat-history')  ? 'active' : '' }}" >
    <a role="button">
        <i class="ion ion-ios-analytics"></i><span>@lang('user-dashboard.investment')</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
		<li class="{{ Request::is('user/investment-new') ? 'active' : '' }}">
			<a href="{!! route('investment-new') !!}"><i class="fa fa-circle-o"></i><span>@lang('user-dashboard.new-investment')</span></a>
		</li>	
		<li class="{{ Request::is('user/investment-history') ? 'active' : '' }}">
			<a href="{!! route('investment-history') !!}"><i class="fa fa-circle-o"></i> <span>@lang('user-dashboard.investment-history')</span></a>
		</li>		
		<li class="{{ Request::is('user/user-repeat-history') ? 'active' : '' }}">
			<a href="{!! route('user-repeat-history') !!}"><i class="fa fa-circle-o"></i><span>@lang('user-dashboard.repeat-history')</span></a>
		</li>
    </ul>
</li>
<li class="treeview {{ Request::is('user/withdraw*') || Request::is('user/deposit*')  || Request::is('user/transaction-log') ? 'active' : '' }}" >
    <a role="button">
        <i class="fa fa-file"></i><span>@lang('user-dashboard.transaction')</span>
         <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
		<li class="{{ Request::is('user/withdraw-request') ? 'active' : '' }}">
			<a href="{!! route('withdraw-request') !!}"><i class="fa fa-circle-o"></i> <span>@lang('user-dashboard.withdraw-fund')</span></a>
		</li>	
		<li class="{{ Request::is('user/withdraw-log') ? 'active' : '' }}">
			<a href="{!! route('withdraw-log') !!}"><i class="fa fa-circle-o"></i> <span>@lang('user-dashboard.withdraw-history')</span></a>
		</li>		
		<li class="{{ Request::is('user/deposit-fund') ? 'active' : '' }}">
			<a href="{!! route('deposit-fund') !!}"><i class="fa fa-circle-o"></i> <span>@lang('user-dashboard.deposit-fund')</span></a>
		</li>		
		<li class="{{ Request::is('user/deposit-history') ? 'active' : '' }}">
			<a href="{!! route('deposit-history') !!}"><i class="fa fa-circle-o"></i> <span>@lang('user-dashboard.deposit-history')</span></a>
		</li>		
		<li class="{{ Request::is('user/transaction-log') ? 'active' : '' }}">
			<a href="{!! route('user-activity') !!}"><i class="fa fa-circle-o"></i> <span>@lang('user-dashboard.transaction-log')</span></a>
		</li>
    </ul>
</li>