@extends('layouts.fontEnd')
@section('content')

    <!--header section start-->
    <section style="background-image: url('{{ asset('assets/images') }}/{{ $basic->breadcrumb }}')" class="breadcrumb-section contact-bg section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1>{{ $page_title}}</h1>
                </div>
            </div>
        </div>
    </section><!--Header section end-->


    <!--login section start-->
    <div class="login-section section-padding login-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="main-login main-center">
                        <a href="{{ route('home') }}"><img src="{{ asset('assets/images/logo.png') }}" alt="Logo Image Will Be Here" style="max-width: 266px;"></a>
                        <br>
                        @if($errors->any())
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {!!  $error !!}
                                </div>
                            @endforeach
                        @endif
                        
                        <form method="post" action="{{ url('/register') }}">

                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="name" class="cols-sm-2 control-label">@lang('register.name')</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="name" value="{{old('name')}}" id="name" required placeholder="@lang('register.enter-name')"/>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="username" class="cols-sm-2 control-label">@lang('register.username')</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="username" value="{{old('username')}}" id="username" required placeholder="@lang('register.enter-username')"/>
                                    </div>
                                </div>
                            </div>

                            @if($reference == '0')
                            <div class="form-group">
                                <label for="name" class="cols-sm-2 control-label">@lang('register.ref-username')</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-code fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="under_reference" id="under_reference" value="@if($reference){{ $reference }}@endif" placeholder="@lang('register.enter-ref-name')"/>
                                    </div>
                                </div>
                            </div>
                            @else
                                <input type="hidden" class="form-control" name="under_reference" id="under_reference" value="@if($reference){{ $reference }}@endif" placeholder="@lang('register.enter-ref-name')"/>
                            @endif

                            <div class="form-group">
                                <label for="email" class="cols-sm-2 control-label">@lang('register.email')</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="email" value="{{old('email')}}" id="email" required placeholder="@lang('register.enter-email')"/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="cols-sm-2 control-label">@lang('register.phone')</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-phone fa" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" name="phone" value="{{old('phone')}}" id="phone" required placeholder="@lang('register.enter-phone')"/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password" class="cols-sm-2 control-label">@lang('register.password')</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                        <input type="password" class="form-control" name="password" id="password" required placeholder="@lang('register.enter-password')"/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="confirm" class="cols-sm-2 control-label">@lang('register.conf-password')</label>
                                <div class="cols-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" required placeholder="@lang('register.enter-conf-password')"/>
                                    </div>
                                </div>
                            </div>

                            @if($basic->google_recap == 1)
                            <div class="form-group">
                                <div class="cols-sm-10">
                                    {!! app('captcha')->display() !!}
                                </div>
                            </div>
                            @endif

                            <div class="form-group ">
                                <button type="submit" class="submit-btn btn btn-lg btn-block login-button">@lang('register.register')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--login section end-->

@endsection
