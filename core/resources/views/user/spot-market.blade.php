@extends('layouts.user-frontend.user-dashboard')
@section('css')
<style>
table.table-markets tr td {
    cursor: pointer!important;
}
</style>
@stop
@section('content')
<div class="content">
	<h4>IDR MARKETS</h4>
	<table class="table table-bordered table-hover table-striped table-markets table-markets-large">
		<thead>
			<tr>
				<th width="17%">Market</th>
				<th width="17%">Asset Name</th>
				<th width="18%">Last Price</th>
				<th width="18%">24h Volume</th>
				<th width="10%">% Change</th>
				<th width="18%">Balance</th>
			</tr>
		</thead>
		<tbody>
			<tr href="{{route('detailMarket', 'BTCIDR')}}" class="market-row  odd">
				<td style="background-color: rgb(249, 249, 249);">BTC/IDR</td>
				<td style="background-color: rgb(249, 249, 249);">Bitcoin</td>
				<td style="background-color: rgb(249, 249, 249);"><span class="price_btcidr_val"></span></td>
				<td style="background-color: rgb(249, 249, 249);"><span class="vol_btcidr_val" title=""></span></td>
				<td style="background-color: rgb(249, 249, 249);"><span class="price_change_btcidr " style="color: rgb(230, 57, 62);">&nbsp;<i class="fa fa-chevron-circle-down"></i> </span></td>
				<td style="background-color: rgb(249, 249, 249);"><span class="balance_btc_val"></span> BTC</td>
			</tr>
			<tr href="{{route('detailMarket','BCHIDR')}}" class="market-row  even">
				<td style="background-color: rgb(255, 255, 255);">BCH/IDR</td>
				<td style="background-color: rgb(255, 255, 255);">Bitcoin Cash</td>
				<td style="background-color: rgb(255, 255, 255);"><span class="price_bchidr_val"></span></td>
				<td style="background-color: rgb(255, 255, 255);"><span class="vol_bchidr_val" title=""></span></td>
				<td style="background-color: rgb(255, 255, 255);"><span class="price_change_bchidr " style="color: rgb(230, 57, 62);">&nbsp;<i class="fa fa-chevron-circle-down"></i></span></td>
				<td style="background-color: rgb(255, 255, 255);"><span class="balance_bch_val"></span> BCH</td>
			</tr>
			<tr href="{{route('detailMarket','BTGIDR')}}" class="market-row  odd">
				<td>BTG/IDR</td>
				<td>Bitcoin Gold</td>
				<td><span class="price_btgidr_val"></span></td>
				<td><span class="vol_btgidr_val" title=""></span></td>
				<td><span class="price_change_btgidr " style="color: rgb(230, 57, 62);">&nbsp;<i class="fa fa-chevron-circle-down"></i> </span></td>
				<td><span class="balance_btg_val"></span> BTG</td>
			</tr>
			<tr href="{{route('detailMarket','ETHIDR')}}" class="market-row  even">
				<td>ETH/IDR</td>
				<td>Ethereum</td>
				<td><span class="price_ethidr_val"></span></td>
				<td><span class="vol_ethidr_val" title=""></span></td>
				<td><span class="price_change_ethidr " style="color: rgb(18, 182, 0);">&nbsp;<i class="fa fa-chevron-circle-up"></i></span></td>
				<td><span class="balance_eth_val"></span> ETH</td>
			</tr>
			<tr href="{{route('detailMarket','ETCIDR')}}" class="market-row  odd">
				<td>ETC/IDR</td>
				<td>Ethereum Classic</td>
				<td><span class="price_etcidr_val"></span></td>
				<td><span class="vol_etcidr_val" title=""></span></td>
				<td><span class="price_change_etcidr " style="color: rgb(230, 57, 62);">&nbsp;<i class="fa fa-chevron-circle-down"></i></span></td>
				<td><span class="balance_etc_val"></span> ETC</td>
			</tr>
			<tr href="{{route('detailMarket','IGNISIDR')}}" class="market-row  even">
				<td style="background-color: rgb(255, 255, 255);">IGNIS/IDR</td>
				<td style="background-color: rgb(255, 255, 255);">IGNIS</td>
				<td style="background-color: rgb(255, 255, 255);"><span class="price_ignisidr_val"></span></td>
				<td style="background-color: rgb(255, 255, 255);"><span class="vol_ignisidr_val" title=""></span></td>
				<td style="background-color: rgb(255, 255, 255);"><span class="price_change_ignisidr " style="color: rgb(18, 182, 0);">&nbsp;<i class="fa fa-chevron-circle-up"></i></span></td>
				<td style="background-color: rgb(255, 255, 255);"><span class="balance_ignis_val"></span> IGNIS</td>
			</tr>
			<tr href="{{route('detailMarket','LTCIDR')}}" class="market-row  odd">
				<td style="background-color: rgb(249, 249, 249);">LTC/IDR</td>
				<td style="background-color: rgb(249, 249, 249);">Litecoin</td>
				<td style="background-color: rgb(249, 249, 249);"><span class="price_ltcidr_val"></span></td>
				<td style="background-color: rgb(249, 249, 249);"><span class="vol_ltcidr_val" title=""></span></td>
				<td style="background-color: rgb(249, 249, 249);"><span class="price_change_ltcidr " style="color: rgb(18, 182, 0);">&nbsp;<i class="fa fa-chevron-circle-up"></i></span></td>
				<td style="background-color: rgb(249, 249, 249);"><span class="balance_ltc_val">0</span> LTC</td>
			</tr>
			<tr href="{{route('detailMarket','TENIDR')}}" class="market-row  odd">
				<td>TEN/IDR</td>
				<td>Tokenomy</td>
				<td><span class="price_tenidr_val"></span></td>
				<td><span class="vol_tenidr_val" title=""></span></td>
				<td><span class="price_change_tenidr " style="color: rgb(230, 57, 62);">&nbsp;<i class="fa fa-chevron-circle-down"></i> </span></td>
				<td><span class="balance_ten_val"></span> TEN</td>
			</tr>
			<tr href="{{route('detailMarket','WAVESIDR')}}" class="market-row  even">
				<td>WAVES/IDR</td>
				<td>Waves</td>
				<td><span class="price_wavesidr_val"></span></td>
				<td><span class="vol_wavesidr_val" title=""></span></td>
				<td><span class="price_change_wavesidr " style="color: rgb(18, 182, 0);">&nbsp;<i class="fa fa-chevron-circle-up"></i></span></td>
				<td><span class="balance_waves_val"></span> WAVES</td>
			</tr>
			<tr href="{{route('detailMarket','XRPIDR')}}" class="market-row  even">
				<td>XRP/IDR</td>
				<td>Ripple</td>
				<td><span class="price_xrpidr_val"></span></td>
				<td><span class="vol_xrpidr_val" title=""></span></td>
				<td><span class="price_change_xrpidr " style="color: rgb(18, 182, 0);">&nbsp;<i class="fa fa-chevron-circle-up"></i></span></td>
				<td><span class="balance_xrp_val"></span> XRP</td>
			</tr>
			<tr href="{{route('detailMarket','XZCIDR')}}" class="market-row  odd">
				<td>XZC/IDR</td>
				<td>ZCoin</td>
				<td><span class="price_xzcidr_val"></span></td>
				<td><span class="vol_xzcidr_val" title=""></span></td>
				<td><span class="price_change_xzcidr " style="color: rgb(18, 182, 0);">&nbsp;<i class="fa fa-chevron-circle-up"></i> </span></td>
				<td><span class="balance_xzc_val"></span> XZC</td>
			</tr>
		</tbody>
	</table>
	<p></p>

	<h4>BTC MARKETS</h4>
	<table class="table table-bordered table-striped table-markets table-markets-large">
	<thead>
		<tr>
			<th width="17%">Market</th>
			<th width="17%">Asset Name</th>
			<th width="18%">Last Price</th>
			<th width="18%">24h Volume</th>
			<th width="10%">% Change</th>
			<th width="18%">Balance</th>
		</tr>
	</thead>
	<tbody>
		<tr href="{{route('detailMarket','BTSBTC')}}" class="">
			<td>BTS/BTC</td>
			<td>BitShares</td>
			<td><span class="price_btsbtc_val"></span></td>
			<td><span class="vol_btsbtc_val" title=""></span></td>
			<td><span class="price_change_btsbtc " style="color: rgb(18, 182, 0);">&nbsp;<i class="fa fa-chevron-circle-up"></i> </span></td>
			<td><span class="balance_bts_val">0</span> BTS</td>
		</tr>
		<tr href="{{route('detailMarket','DASHBTC')}}" class="">
			<td>DASH/BTC</td>
			<td>DASH</td>
			<td><span class="price_drkbtc_val"></span></td>
			<td><span class="vol_drkbtc_val" title="1,5 BTC"></span></td>
			<td><span class="price_change_drkbtc " style="color: rgb(18, 182, 0);">&nbsp;<i class="fa fa-chevron-circle-up"></i> </span></td>
			<td><span class="balance_drk_val">0</span> DASH</td>
		</tr>
		<tr href="{{route('detailMarket','DOGEBTC')}}" class="">
			<td>DOGE/BTC</td>
			<td>Dogecoin</td>
			<td><span class="price_dogebtc_val"></span></td>
			<td><span class="vol_dogebtc_val" title=""></span></td>
			<td><span class="price_change_dogebtc " style="color: rgb(230, 57, 62);">&nbsp;<i class="fa fa-chevron-circle-down"></i></span></td>
			<td><span class="balance_doge_val">0</span> DOGE</td>
		</tr>
		<tr href="{{route('detailMarket','ETHBTC')}}" class="">
			<td>ETH/BTC</td>
			<td>Ethereum</td>
			<td><span class="price_ethbtc_val"></span></td>
			<td><span class="vol_ethbtc_val" title=""></span></td>
			<td><span class="price_change_ethbtc " style="color: rgb(18, 182, 0);">&nbsp;<i class="fa fa-chevron-circle-up"></i></span></td>
			<td><span class="balance_eth_val">0</span> ETH</td>
		</tr>
		<tr href="{{route('detailMarket','LTCBTC')}}" class="">
			<td>LTC/BTC</td>
			<td>Litecoin</td>
			<td><span class="price_ltcbtc_val"></span></td>
			<td><span class="vol_ltcbtc_val" title=""></span></td>
			<td><span class="price_change_ltcbtc " style="color: rgb(18, 182, 0);">&nbsp;<i class="fa fa-chevron-circle-up"></i> </span></td>
			<td><span class="balance_ltc_val">0</span> LTC</td>
		</tr>
		<tr href="{{route('detailMarket','XEMBTC')}}" class="">
			<td>XEM/BTC</td>
			<td>NEM</td>
			<td><span class="price_nembtc_val"></span></td>
			<td><span class="vol_nembtc_val" title=""></span></td>
			<td><span class="price_change_nembtc " style="color: rgb(18, 182, 0);">&nbsp;<i class="fa fa-chevron-circle-up"></i></span></td>
			<td><span class="balance_nem_val">0</span> XEM</td>
		</tr>
		<tr href="{{route('detailMarket','XRPBTC')}}" class="">
			<td>XRP/BTC</td>
			<td>Ripple</td>
			<td><span class="price_xrpbtc_val"></span></td>
			<td><span class="vol_xrpbtc_val" title=""></span></td>
			<td><span class="price_change_xrpbtc " style="color: rgb(18, 182, 0);">&nbsp;<i class="fa fa-chevron-circle-up"></i></span></td>
			<td><span class="balance_xrp_val">0</span> XRP</td>
		</tr>
	</tbody>
	</table>
	<p></p>
	<!-- <h4>USD MARKETS</h4>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Market</th>
				<th>Asset Name</th>
				<th>Last Price</th>
				<th>24h Volume</th>
				<th>% Change</th>
				<th>Balance</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>XX</td>
				<td>XX</td>
				<td>XX</td>
				<td>XX</td>
				<td>XX</td>
				<td>XX</td>
			</tr>
		</tbody>
	</table> -->
</div>
@endsection
@section('scripts')
<script>
	
    var pusher = new Pusher('a0dfa181b1248b929b11', {
      cluster: 'ap1',
      encrypted: true
    });

    var update_tradedata = function(data, first_load){
        update_price_all(data.prices);
        update_volumes(data.volumes);
        update_price_arrow(data.prices, data.prices_24h);
        last_price = data.last_price;
        high_price = data._24h.high;
        low_price = data._24h.low;
    }

    var pusher_tradedata = false;
    var start_pusher_tradedata = function(){
        if(pusher_tradedata) return;
        pusher_tradedata = pusher.subscribe('tradedata-btcidr');
        pusher_tradedata.bind('update', function(data) {
            console.log('update', data);
            update_tradedata(data);
        });
    }
    
    var stop_pusher_tradedata = function(){
        if(!pusher_tradedata) return;
        pusher_tradedata.unbind('update');
        pusher_tradedata.unsubscribe('tradedata-btcidr');
        pusher_tradedata = false;
    }
    
    $(function(){
        start_pusher_tradedata();
        
        $(document).idle({
            onHide: function(){
                stop_pusher_tradedata();
            },
            onShow: function(){
                start_pusher_tradedata();
            }
        });
    });

    if (typeof up_to_date === "function") { 
        setInterval(up_to_date, 60000);
    }
$(function(){
	$('.table-markets-large tr').click(function(){
		if($(this).attr('href')) window.location = $(this).attr('href');
	});
});
</script>
@endsection