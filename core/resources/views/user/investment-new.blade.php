@extends('layouts.user-frontend.user-dashboard')
@section('content')

    <div class="content">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="page_title">{!! $page_title  !!} </h3>
                    <hr>
                </div>
            </div>
            <div class="row">

                @foreach($plan as $p)

                    <div class="col-sm-4 text-center">
                        <div class="panel panel-warning panel-pricing">
                            <div class="panel-heading">
                                <h3 style="font-size: 28px;"><b>{{ $p->name }}</b></h3>
                            </div>
                            <div style="font-size: 18px;padding: 18px;" class="panel-body text-center">
                                <p><strong>{{ $p->minimum }} {{ $basic->currency }} - {{ $p->maximum }} {{ $basic->currency }}</strong></p>
                            </div>
                            <ul style='font-size: 15px;' class="list-group text-center bold">
                                <li class="list-group-item"><i class="fa fa-check"></i> Commission - {{ $p->percent }} <i class="fa fa-percent"></i> </li>
                                <li class="list-group-item"><i class="fa fa-check"></i> Repeat - {{ $p->time }} times </li>
                                <li class="list-group-item"><i class="fa fa-check"></i> Compound - <span class="aaaa">{{ $p->compound->name }}</span></li>
                            </ul>
                            <div class="panel-footer" style="overflow: hidden">
                                <div class="col-sm-12">
                                    <form method="POST" action="{{ route('investment-post') }}" class="form-inline">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="id" value="{{ $p->id }}">
                                        <button type="submit" class="btn btn-warning bold uppercase btn-block btn-icon icon-left">
                                            <i class="fa fa-send"></i> Invest Under This Package
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div><!---ROW-->
@endsection
@section('scripts')

    @if (session('success'))
        <script type="text/javascript">
            $(document).ready(function(){

                swal("Success!", "{{ session('success') }}", "success");

            });
        </script>

    @endif

    @if (session('alert'))

        <script type="text/javascript">
            $(document).ready(function(){
                swal("Sorry!", "{{ session('alert') }}", "error");
            });

        </script>

    @endif

@endsection

