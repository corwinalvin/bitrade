@extends('layouts.user-frontend.user-dashboard')
@section('content')
<div class="content">
	<div class="portlet light bordered">
		<div class="portlet-title">
			<div class="caption font-dark"><strong>{{$page_title}}</strong>
			</div>
			<div class="tools"> </div>
		</div>
		<div class="portlet-body">
			@if(count($errors) > 0)
			   @foreach ($errors->all() as $error)
			      <div>{{ $error }}</div>
			  @endforeach
			@endif
            <div class="qrcode">
                Google QRCode
            </div>
			Open up your 2FA mobile app and scan the following QR barcode:
			<br />
                <img src="{{ $googleUrl }}" alt="">
			<br />
			If your 2FA mobile app does not support QR barcodes,
			enter in the following number: <code>{{ $key }}</code>
			<br /><br />
			 	<form action="{{route('2fa.validate')}}" method="post">
                    Type your code: <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="text" name="google2fa_secret">
                    <input type="submit" value="check">
                </form>
		</div>
	</div>
</div>
	@stop