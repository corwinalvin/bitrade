@extends('layouts.user-frontend.user-dashboard')
@section('content')
<div class="content">
	<div class="portlet light bordered">
		<div class="portlet-title">
			<div class="caption font-dark"><strong>{{$page_title}}</strong>
			</div>
			<div class="tools"> </div>
		</div>
		<div class="portlet-body">
			2FA has been removed
            <br /><br />
            <a href="{{ url('/home') }}">Go Home</a>
		</div>
	</div>	
</div>               
@stop