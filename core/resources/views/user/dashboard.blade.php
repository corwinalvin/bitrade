@extends('layouts.user-frontend.user-dashboard')
@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif
            <a href="{{ route('deposit-history') }}">
                <div class="col-md-3">
                    <div class="dashboard-stat blue">
                        <div class="visual">
                            <i class="fa fa-cloud-download"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{ $basic->symbol }}   <span data-counter="counterup" data-value="{{ $deposit }}">{{ $deposit }}</span>
                            </div>
                            <div class="desc bold">@lang('user-dashboard.total-deposit')</div>
                        </div>
                    </div>
                </div>
            </a>

            <a href="{{ route('withdraw-log') }}">
                <div class="col-md-3">
                    <div class="dashboard-stat blue">
                        <div class="visual">
                            <i class="fa fa-cloud-upload"></i>
                        </div>
                        <div class="details">
                            <div class="number">
                                {{ $basic->symbol }} <span data-counter="counterup" data-value="{{ $withdraw }}">{{ $withdraw }}</span>
                            </div>
                            <div class="desc bold">@lang('user-dashboard.total-withdraw')</div>
                        </div>
                    </div>
                </div>
            </a>

            <a href="{{ route('user-activity') }}">
                <div class="col-md-3">
                    <div class="dashboard-stat yellow">
                        <div class="visual">
                            <i class="fa fa-bitcoin"></i>
                        </div>
                        <div class="details" >
                            <div class="number" style="border-bottom: 1px solid #FFF">
                                <!-- <span data-counter="counterup" data-value="{{ round($balance, $basic->deci) }}">{{ round($balance, $basic->deci) }} BTC</span> -->
                                <span>BTC</span>
                            </div>
                            <div class="desc bold"> @lang('user-dashboard.balance-bitcoin') </div>
                        </div>
                    </div>
                </div>
            </a>            

            <a href="{{ route('user-activity') }}">
                <div class="col-md-3">
                    <div class="dashboard-stat green">
                        <div class="visual">
                            <i class="fa fa-money"></i>
                        </div>
                        <div class="details">
                            <div class="number" style="border-bottom: 1px solid #FFF">
                                  {{ $basic->symbol }}  <span data-counter="counterup" data-value="{{ round($balance, $basic->deci) }}">{{ number_format(round($balance, $basic->deci)) }}</span>
                            </div>
                            <div class="desc bold"> @lang('user-dashboard.balance-rupiah')</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-6">
        <h4>@lang('user-dashboard.buy-bitcoin')</h4>
        <!-- <div class="panel panel-default"> -->
        <form method="post" id="wizard-beli" class="panel-wizard">
        <ul class="nav nav-justified nav-wizard mt0 nav-disabled-click nav-pills">
            <li><a href="#tab1-beli" data-toggle="tab"><strong>Step 1:</strong> Input</a></li>
            <li><a href="#tab2-beli" data-toggle="tab" disable><strong>Step 2:</strong> @lang('user-dashboard.confirm')</a></li>
            <li><a href="#tab3-beli" data-toggle="tab"><strong>Step 3:</strong> @lang('user-dashboard.result')</a></li>
        </ul>
    <div class="tab-content">
        <div class="tab-pane" id="tab1-beli">
            <div class="form-group">
                <label class="col-sm-4">@lang('user-dashboard.balance-rupiah'):</label>
                <div class="col-sm-8">
                    <a href="#" class="balance_rp_val" onclick="$('#instan_rp_val').val(balance_rp).change(); return false;">{{$balance}}</a>
                    <span class="deposit_idr_link">
                        &nbsp;
                        [ <a href="{{route('deposit-fund')}}" class="underline">@lang('user-dashboard.click-deposit-rupiah')</a> ]
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 middle-label">@lang('user-dashboard.amount-in-rupiah'):</label>
                <div class="col-sm-8">
                    <div class="input-group form-horizontal">
                        <input type="text" name="rp" class="form-control rp_formatter" placeholder="Jumlah Rupiah" id="instan_rp_val">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
                            <i class="fa fa-caret-left"></i>
                            </button>
                            <ul class="dropdown-menu amount-percent" role="menu">
                                <li><a href="#" onclick="$('#instan_rp_val').val(balance_rp*0.1).change(); return false;">10%</a></li>
                                <li><a href="#" onclick="$('#instan_rp_val').val(balance_rp*0.25).change(); return false;">25%</a></li>
                                <li><a href="#" onclick="$('#instan_rp_val').val(balance_rp*0.5).change(); return false;">50%</a></li>
                                <li><a href="#" onclick="$('#instan_rp_val').val(balance_rp*0.75).change(); return false;">75%</a></li>
                                <li><a href="#" onclick="$('#instan_rp_val').val(balance_rp).change(); return false;">100%</a></li>
                            </ul>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4">Harga 1 BTC:</label>
                <div class="col-sm-8">
                Harga akan dikalkulasi sesuai dengan harga market pada saat order dipasang. Klik "Lanjutkan" untuk mendapatkan estimasi harga. </div>
            </div>
        </div>
        <div class="tab-pane" id="tab2-beli">
            <div class="form-group">
                <div class="col-sm-12">
                @lang('user-dashboard.desc-1') </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4">@lang('user-dashboard.amount-in-rupiah'):</label>
                <div class="col-sm-8" id="instan_buy_confirm_rp">
                    <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4">@lang('user-dashboard.estimationofprice'):</label>
                <div class="col-sm-8" id="instan_buy_confirm_harga">
                    <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4">@lang('user-dashboard.fee'):</label>
                <div class="col-sm-8" id="instan_buy_confirm_fee">
                    <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4">@lang('user-dashboard.estimationofbought'):</label>
                <div class="col-sm-8" id="instan_buy_confirm_btc">
                    <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                @lang('user-dashboard.desc-2') </div>
            </div>
        </div>
        <div class="tab-pane" id="tab3-beli">
            <div class="form-group hide" id="instan_buy_hasil_loading">
                <div class="col-sm-12">
                    <center><img src="https://indodax.com/v2/images/loaders/loader8.gif" style="margin: 70px 0"></center>
                </div>
            </div>
            <div id="instan_buy_hasil" class="hide form-horizontal">
                <div class="form-group">
                    <div class="col-sm-12">
                    Selamat, pembelian Bitcoin telah berhasil! Berikut adalah rinciannya. </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">Rupiah Terpotong:</label>
                    <div class="col-sm-8" id="instan_buy_hasil_rp">
                        <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">Bitcoin Didapatkan:</label>
                    <div class="col-sm-8" id="instan_buy_hasil_btc">
                        <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">Biaya:</label>
                    <div class="col-sm-8" id="instan_buy_hasil_fee">
                        <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">Harga Rata-Rata:</label>
                    <div class="col-sm-8" id="instan_buy_hasil_harga">
                        <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">Waktu:</label>
                    <div class="col-sm-8" id="instan_buy_hasil_waktu">
                        <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <ul class="list-unstyled wizard">
        <li class="pull-left previous disabled hide"><button type="button" class="btn btn-default">Kembali</button></li>
        <li class="pull-right next"><button type="button" class="btn btn-primary">@lang('user-dashboard.continue')</button></li>
        <li class="pull-right finish hide"><button type="button" class="btn btn-primary" onclick="instan_buy_selesai(); return false;">Selesai</button></li>
    </ul>
  </form>
</div>
    <div class="col-lg-6">
        <h4>@lang('user-dashboard.sell-bitcoin')</h4>
        <div class="panel panel-default">
        <form method="post" id="wizard-jual" class="panel-wizard">
             <ul class="nav nav-justified nav-wizard mt0 nav-disabled-click nav-pills">
<li class="active"><a href="#tab1-beli" data-toggle="tab"><strong>Step 1:</strong> Input</a></li>
<li><a href="#tab2-beli" data-toggle="tab"><strong>Step 2:</strong> @lang('user-dashboard.confirm')</a></li>
<li><a href="#tab3-beli" data-toggle="tab"><strong>Step 3:</strong> @lang('user-dashboard.result')</a></li>
</ul>
</form>

          <div class="panel-body">
            <form class="form-horizontal">
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-4">@lang('user-dashboard.balance-bitcoin'):</label>
                <div class="col-sm-8">
                    <a href="#" class="balance_rp_val" onclick="$('#instan_rp_val').val(balance_rp).change(); return false;">0</a>
                    <span class="deposit_idr_link">
                    &nbsp;
                    [ <a href="https://vip.bitcoin.co.id/rupiah" class="underline">@lang('user-dashboard.click-deposit-rupiah')</a> ]
                    </span>
                </div>
              </div>
              <div class="form-group">
                <label for="inputPassword3" class="col-sm-4">@lang('user-dashboard.amount-in-rupiah')</label>
                <div class="col-sm-8">
                 <div class="input-group">
<input type="text" name="btc" class="form-control btc_formatter" placeholder="Jumlah Bitcoin" id="instan_btc_val">
<span class="input-group-btn">
<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
<i class="fa fa-caret-left"></i>
</button>
<ul class="dropdown-menu amount-percent" role="menu">
<li><a href="#" onclick="$('#instan_btc_val').val(btc_format(balance_btc*0.1)).change(); return false;">10%</a></li>
<li><a href="#" onclick="$('#instan_btc_val').val(btc_format(balance_btc*0.25)).change(); return false;">25%</a></li>
<li><a href="#" onclick="$('#instan_btc_val').val(btc_format(balance_btc*0.5)).change(); return false;">50%</a></li>
<li><a href="#" onclick="$('#instan_btc_val').val(btc_format(balance_btc*0.75)).change(); return false;">75%</a></li>
<li><a href="#" onclick="$('#instan_btc_val').val(btc_format(balance_btc)).change(); return false;">100%</a></li>
</ul>
</span>
</div>
                </div>
              </div>
            </form>
          </div>
          <div class="panel-footer">
            <!-- <div class="pull-right"> -->
              <button type="submit" class="col-sm-offset-10 btn btn-default">@lang('user-dashboard.continue')</button>
            <!-- </div> -->
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h3 class="page_title">{!! $reference_title  !!} </h3>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                        <thead>
                            <tr>
                                <th>ID#</th>
                                <th>Register Date</th>
                                <th>User Name</th>
                                <th>Username</th>
                                <th>User Email</th>
                                <th>User Phone</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php $i=0;@endphp
                        @foreach($reference_user as $p)
                            @php $i++;@endphp
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ date('d-F-Y h:i A',strtotime($p->created_at))  }}</td>
                                <td>{{ $p->name }}</td>
                                <td>{{ $p->username }}</td>
                                <td>{{ $p->email }}</td>
                                <td>{{ $p->phone }}</td>
                                <td>
                                    @if($p->status == 1)
                                        <span class="label bold label-danger bold uppercase"><i class="fa fa-user-times"></i> Blocked</span>
                                    @else
                                        <span class="label bold label-success bold uppercase"><i class="fa fa-check"></i> Active</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div><!-- ROW-->
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">

                <label><strong>@lang('user-dashboard.referal-link'):</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>@lang('user-dashboard.amount-refer') : {{ $refer }} </strong></label>
                <div class="input-group mb15">
                    <input type="text" class="form-control input-lg" id="ref" value="{{ route('auth.reference-register',Auth::user()->username) }}"/>
                    <span class="input-group-btn">
                        <button data-copytarget="#ref" class="btn btn-success btn-lg">@lang('user-dashboard.copy')</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $('.has').tooltip({
            trigger: 'click',
            placement: 'bottom'
        });

        function setTooltip(btn, message) {
            $(btn).tooltip('hide')
                    .attr('data-original-title', message)
                    .tooltip('show');
        }

        function hideTooltip(btn) {
            setTimeout(function() {
                $(btn).tooltip('hide');
            }, 1000);
        }

        // Clipboard
        $(document).ready(function () {

            $(document).on("click", '.delete_button', function (e) {
                var id = $(this).data('id');
                $(".abir_id").val(id);

            });

        });
        $('#btnYes').click(function() {
            $('#formSubmit').submit();
        });
    </script>
    <script src="{{ asset('assets/admin/js/clipboard.min.js') }}"></script>
    <script>
        (function() {

            'use strict';

            // click events
            document.body.addEventListener('click', copy, true);

            // event handler
            function copy(e) {

                // find target element
                var
                        t = e.target,
                        c = t.dataset.copytarget,
                        inp = (c ? document.querySelector(c) : null);

                // is element selectable?
                if (inp && inp.select) {

                    // select text
                    inp.select();

                    try {
                        // copy text
                        document.execCommand('copy');
                        inp.blur();

                        // copied animation
                        t.classList.add('copied');
                        setTimeout(function() { t.classList.remove('copied'); }, 1500);
                    }
                    catch (err) {
                        alert('please press Ctrl/Cmd+C to copy');
                    }

                }

            }

        })();

    </script>
<script type="text/javascript">
jQuery('#wizard-beli').bootstrapWizard({
    onTabShow: function(tab, navigation, index) {
        tab.prevAll().addClass('done');
        tab.nextAll().removeClass('done');
        tab.removeClass('done');
                    
        var $total = navigation.find('li').length;
        var $current = index + 1;
        
        var instan_rp_val = ($('#instan_rp_val').val()+'').replace(/[^0-9]/g,'');
        
        if($current==1){ 
            $('#wizard-beli').find('.wizard .previous').addClass('hide');
            $('#wizard-beli').find('.wizard .next').removeClass('hide');
            $('#wizard-beli').find('.wizard .finish').addClass('hide');
        }
        
        else if($current==2){ //konfirmasi
            $('#wizard-beli').find('.wizard .previous').removeClass('hide');
            $('#wizard-beli').find('.wizard .next').addClass('hide');
            $('#wizard-beli').find('.wizard .finish').addClass('hide');
            
            $('#instan_buy_confirm_rp, #instan_buy_confirm_harga, #instan_buy_confirm_btc').html('<img src="https://indodax.com/v2/images/loaders/loader8.gif" />');
            
            $('#instan_buy_confirm_rp').html(rp_format(instan_rp_val));
            
            $.ajax({
                url: API_BASE+'api/btc_idr/depth',
                dataType: 'json',
                type: 'post'
            })
            .done(function(data) {
                
                var rp = instan_rp_val;
                
                buy_fee = Math.floor(rp-(rp/(1+trade_fee)));
                var rp_remain = rp-buy_fee;
                var btc_got = 0;
                var estimation_last_price = false;
                var sell_orders = data.sell;
                var sum_rp = 0;
                var sum_btc = 0;
                
                var index;
                for (index = 0; index < sell_orders.length; ++index) {
                    sum_btc = sell_orders[index][1]*1;
                    sum_rp = sell_orders[index][1]*sell_orders[index][0];
                    if(rp_remain>sum_rp){
                        rp_remain -= sum_rp;
                        btc_got += sum_btc;
                    }
                    else{
                        btc_got += rp_remain/sell_orders[index][0];
                        estimation_last_price = sell_orders[index][0];
                        rp_remain = 0;
                        break;
                    }
                }
                
                if(!rp_remain){ 
                    btc_got = (Math.floor(btc_got*100000000)/100000000)+'';
                    $('#instan_buy_confirm_btc').html(btc_got.replace(thousep, decpoint)+' BTC');
                    $('#instan_buy_confirm_harga').html(rp_format((rp-buy_fee)/btc_got));
                    $('#instan_buy_confirm_fee').html(rp_format(buy_fee));
                    $('#wizard-beli').find('.wizard .next').removeClass('hide');
                    
                    buy_confirm_timeout = Math.floor(Date.now() / 1000)+60;
                    
                    return false;
                }
                else{ 
                    alert_error('Transaksi terlalu besar untuk metode instan. Silakan gunakan Marketplace.');
                    $('#wizard-beli').bootstrapWizard('previous');
                    return false;
                }
                
            })
            .fail(function() {
                alert_error( "Server tidak merespon. Mohon cek koneksi internet anda.\nServer not responding. Please check your internet connection." );
            })
            .always(function() {
            });
            
        }
        
        else if($current==3){ //hasil
            $('#wizard-beli').find('.wizard .previous').addClass('hide');
            $('#wizard-beli').find('.wizard .next').addClass('hide');
            
            $('#instan_buy_hasil_loading').removeClass('hide');
            $('#instan_buy_hasil').addClass('hide');
            
            $.ajax({
                url: ROOT+'market/ajax/buy',
                data: {
                    pair: 'btcidr',
                    buy_amount_rp: instan_rp_val,
                    method: 'market'
                },
                dataType: 'json',
                type: 'post'
            })
            .done(function(data) {
                if(data.is_error){ 
                    alert_error(data.error);
                    $('#wizard-beli').bootstrapWizard('previous');
                    return;
                }
                
                $('#instan_buy_hasil_rp').html(rp_format(data.total_trade_rp+data.total_fee_rp));
                $('#instan_buy_hasil_btc').html(btc_format(data.total_trade_btc/1e8)+' BTC');
                $('#instan_buy_hasil_fee').html(rp_format(data.total_fee_rp));
                $('#instan_buy_hasil_harga').html(rp_format(data.total_trade_rp/data.total_trade_btc*1e8));
                $('#instan_buy_hasil_waktu').html(data.time);
                
                $('#instan_buy_hasil_loading').addClass('hide');
                $('#instan_buy_hasil').removeClass('hide');
                $('#wizard-beli').find('.wizard .finish').removeClass('hide');
                
                up_to_date();
            })
            .fail(function() {
                alert_error( "Server tidak merespon. Mohon cek koneksi internet anda.\nServer not responding. Please check your internet connection." );
            })
            .always(function() {
            });            
        }
    },
    onNext: function(tab, navigation, index){
        var $current = index + 1;
        var instan_rp_val = ($('#instan_rp_val').val()+'').replace(/[^0-9]/g,'')*1;
        if($current>=2){
            if(instan_rp_val<trade_rp_min){
                alert_error('Minimal pembelian adalah Rp. '+rp_format(trade_rp_min));
                return false;
            }
            if(instan_rp_val>balance_rp){
                alert_error('Saldo tidak mencukupi.');
                return false;
            }
        }
        if($current==3){
            if(buy_confirm_timeout<Math.floor(Date.now() / 1000)){
                alert_error('Order telah kadaluarsa (melewati 60 detik). Silakan ulangi dari step 1.');
                $('#wizard-beli').bootstrapWizard('previous');
                return false;
            }
        }
        
    },
    onTabClick: function(tab, navigation, index) {
        return false;
    }
});
</script>
@endsection