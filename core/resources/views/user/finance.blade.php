@extends('layouts.user-frontend.user-dashboard')
@section('content')
<div class="content">
   <div class="table-responsive">
      <h4 class="pull-left">FINANCE</h4>
      <div class="input-group mb5 pull-right" style="width: 225px;">
         <span class="input-group-addon"><i class="fa fa-search"></i></span>
         <input type="text" class="form-control" placeholder="Search" name="gauth_pin" value="" autocomplete="off" id="search_table_finance">
      </div>
      <div class="clear"></div>
      <div id="finance-table_wrapper" class="dataTables_wrapper">
         <table id="finance-table" class="table table-bordered table-striped table-markets-large dataTable" role="grid">
            <thead>
               <tr role="row">
                  <th width="8%" class="sorting_disabled" rowspan="1" colspan="1" style="width: 62px;">Code</th>
                  <th width="16%" class="sorting_disabled" rowspan="1" colspan="1" style="width: 145px;">Asset Name</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 208px;">Balance</th>
                  <th class="sorting_disabled" rowspan="1" colspan="1" style="width: 230px;">Est. Rupiah Value</th>
                  <th width="30%" class="sorting_disabled" rowspan="1" colspan="1" style="width: 292px;">Action</th>
               </tr>
            </thead>
            <tbody>
               <tr class="odd" role="row">
                  <td><strong><a href="https://vip.bitcoin.co.id/rupiah">IDR</a></strong></td>
                  <td><a href="https://vip.bitcoin.co.id/rupiah">Rupiah</a></td>
                  <td>0 IDR</td>
                  <td class="text_right">0 IDR</td>
                  <td>
                     <button onclick="window.location='{{route("deposit-fund")}}';" class="btn btn-xs btn-info btn-wd-depo">Deposit / Withdraw IDR </button>
                  </td>
               </tr>
               <tr class="even" role="row">
                  <td><strong><a href="">ACN</a></strong></td>
                  <td><a href="{{route("detailFinance","acn")}}">Activa Coin</a></td>
                  <td>{{$balance_acn}} ACN</td>
                  <td class="text_right">0 IDR</td>
                  <td class="">
                     <button onclick="window.location='{{route("detailFinance", "acn")}}';" class="btn btn-xs btn-info btn-wd-depo">Deposit / Withdraw ACN</button>
                     <button onclick="window.location='{{route("detailMarket","ACNIDR")}}';" class="btn btn-xs btn-warning btn-market">Market ACN</button>
                  </td>
               </tr>
               <tr class="even" role="row">
                  <td><strong><a href="">BTC</a></strong></td>
                  <td><a href="{{route("detailFinance", "btc")}}">Bitcoin</a></td>
                  <td>0 BTC</td>
                  <td class="text_right">0 IDR</td>
                  <td class="">
                     <button onclick="window.location='{{route("detailFinance","btc")}}';" class="btn btn-xs btn-info btn-wd-depo">Deposit / Withdraw BTC</button>
                     <button onclick="window.location='{{route("detailMarket", "BTCIDR")}}';" class="btn btn-xs btn-warning btn-market">Market BTC</button>
                  </td>
               </tr> 
               <tr class="odd" role="row">
                  <td><strong><a href="">BCH</a></strong></td>
                  <td><a href="{{route("detailFinance", "bch")}}">Bitcoin Cash</a></td>
                  <td>0 BCH</td>
                  <td class="text_right">0 IDR</td>
                  <td class="">
                     <button onclick="window.location='{{route("detailFinance","bch")}}';" class="btn btn-xs btn-info btn-wd-depo">Deposit / Withdraw BCH</button>
                     <button onclick="window.location='{{route("detailMarket","BCHIDR")}}';" class="btn btn-xs btn-warning btn-market">Market BCH</button>
                  </td>
               </tr>
               <tr class="even" role="row">
                  <td><strong><a href="">BTG</a></strong></td>
                  <td><a href="{{route("detailFinance", "btg")}}">Bitcoin Gold</a></td>
                  <td>0 BTG</td>
                  <td class="text_right">0 IDR</td>
                  <td class="">
                     <button onclick="window.location='{{route("detailFinance", "btg")}}';" class="btn btn-xs btn-info btn-wd-depo">Deposit / Withdraw BTG</button>
                     <button onclick="window.location='{{route("detailMarket", "BTGIDR")}}';" class="btn btn-xs btn-warning btn-market">Market BTG</button>
                  </td>
               </tr>
              <!--  <tr class="odd" role="row">
                  <td><strong><a href="">BTS</a></strong></td>
                  <td><a href="https://vip.bitcoin.co.id/finance/bts">BitShares</a></td>
                  <td>0 BTS</td>
                  <td class="text_right">0 IDR</td>
                  <td class="">
                     <button onclick="window.location='https://vip.bitcoin.co.id/finance/bts';" class="btn btn-xs btn-info btn-wd-depo">Deposit / Withdraw BTS</button>
                     <button onclick="window.location='https://vip.bitcoin.co.id/market/BTSBTC';" class="btn btn-xs btn-warning btn-market">Market BTS</button>
                  </td>
               </tr>
               <tr class="even" role="row">
                  <td><strong><a href="">DASH</a></strong></td>
                  <td><a href="https://vip.bitcoin.co.id/finance/drk">DASH</a></td>
                  <td>0 DASH</td>
                  <td class="text_right">0 IDR</td>
                  <td class="">
                     <button onclick="window.location='https://vip.bitcoin.co.id/finance/drk';" class="btn btn-xs btn-info btn-wd-depo">Deposit / Withdraw DASH</button>
                     <button onclick="window.location='https://vip.bitcoin.co.id/market/DASHBTC';" class="btn btn-xs btn-warning btn-market">Market DASH</button>
                  </td>
               </tr>
               <tr class="odd" role="row">
                  <td><strong><a href="">DOGE</a></strong></td>
                  <td><a href="https://vip.bitcoin.co.id/finance/doge">Dogecoin</a></td>
                  <td>0 DOGE</td>
                  <td class="text_right">0 IDR</td>
                  <td class="">
                     <button onclick="window.location='https://vip.bitcoin.co.id/finance/doge';" class="btn btn-xs btn-info btn-wd-depo">Deposit / Withdraw DOGE</button>
                     <button onclick="window.location='https://vip.bitcoin.co.id/market/DOGEBTC';" class="btn btn-xs btn-warning btn-market">Market DOGE</button>
                  </td>
               </tr>
               <tr class="even" role="row">
                  <td><strong><a href="">ETH</a></strong></td>
                  <td><a href="https://vip.bitcoin.co.id/finance/eth">Ethereum</a></td>
                  <td>0 ETH</td>
                  <td class="text_right">0 IDR</td>
                  <td class="">
                     <button onclick="window.location='https://vip.bitcoin.co.id/finance/eth';" class="btn btn-xs btn-info btn-wd-depo">Deposit / Withdraw ETH</button>
                     <button onclick="window.location='https://vip.bitcoin.co.id/market/ETHIDR';" class="btn btn-xs btn-warning btn-market">Market ETH</button>
                  </td>
               </tr>
               <tr class="odd" role="row">
                  <td><strong><a href="">ETC</a></strong></td>
                  <td><a href="https://vip.bitcoin.co.id/finance/etc">Ethereum Classic</a></td>
                  <td>0 ETC</td>
                  <td class="text_right">0 IDR</td>
                  <td class="">
                     <button onclick="window.location='https://vip.bitcoin.co.id/finance/etc';" class="btn btn-xs btn-info btn-wd-depo">Deposit / Withdraw ETC</button>
                     <button onclick="window.location='https://vip.bitcoin.co.id/market/ETCIDR';" class="btn btn-xs btn-warning btn-market">Market ETC</button>
                  </td>
               </tr>
               <tr class="even" role="row">
                  <td><strong><a href="">IGNIS</a></strong></td>
                  <td><a href="https://vip.bitcoin.co.id/finance/ignis">IGNIS</a></td>
                  <td>0 IGNIS</td>
                  <td class="text_right">0 IDR</td>
                  <td class="">
                     <button onclick="window.location='https://vip.bitcoin.co.id/finance/ignis';" class="btn btn-xs btn-info btn-wd-depo">Deposit / Withdraw IGNIS</button>
                     <button onclick="window.location='https://vip.bitcoin.co.id/market/IGNISIDR';" class="btn btn-xs btn-warning btn-market">Market IGNIS</button>
                  </td>
               </tr>
               <tr class="odd" role="row">
                  <td><strong><a href="">LTC</a></strong></td>
                  <td><a href="https://vip.bitcoin.co.id/finance/ltc">Litecoin</a></td>
                  <td>0 LTC</td>
                  <td class="text_right">0 IDR</td>
                  <td class="">
                     <button onclick="window.location='https://vip.bitcoin.co.id/finance/ltc';" class="btn btn-xs btn-info btn-wd-depo">Deposit / Withdraw LTC</button>
                     <button onclick="window.location='https://vip.bitcoin.co.id/market/LTCIDR';" class="btn btn-xs btn-warning btn-market">Market LTC</button>
                  </td>
               </tr>
               <tr class="even" role="row">
                  <td><strong><a href="">NXT</a></strong></td>
                  <td><a href="https://vip.bitcoin.co.id/finance/nxt">NXT</a></td>
                  <td>0 NXT</td>
                  <td class="text_right">0 IDR</td>
                  <td class="">
                     <button onclick="window.location='https://vip.bitcoin.co.id/finance/nxt';" class="btn btn-xs btn-info btn-wd-depo">Deposit / Withdraw NXT</button>
                     <button onclick="window.location='https://vip.bitcoin.co.id/market/NXTIDR';" class="btn btn-xs btn-warning btn-market">Market NXT</button>
                  </td>
               </tr>
               <tr class="odd" role="row">
                  <td><strong><a href="">XEM</a></strong></td>
                  <td><a href="https://vip.bitcoin.co.id/finance/nem">NEM</a></td>
                  <td>0 XEM</td>
                  <td class="text_right">0 IDR</td>
                  <td class="">
                     <button onclick="window.location='https://vip.bitcoin.co.id/finance/nem';" class="btn btn-xs btn-info btn-wd-depo">Deposit / Withdraw XEM</button>
                     <button onclick="window.location='https://vip.bitcoin.co.id/market/XEMBTC';" class="btn btn-xs btn-warning btn-market">Market XEM</button>
                  </td>
               </tr>
               <tr class="even" role="row">
                  <td><strong><a href="">XLM</a></strong></td>
                  <td><a href="https://vip.bitcoin.co.id/finance/str">Stellar Lumens</a></td>
                  <td>0 XLM</td>
                  <td class="text_right">0 IDR</td>
                  <td class="">
                     <button onclick="window.location='https://vip.bitcoin.co.id/finance/str';" class="btn btn-xs btn-info btn-wd-depo">Deposit / Withdraw XLM</button>
                     <button onclick="window.location='https://vip.bitcoin.co.id/market/XLMIDR';" class="btn btn-xs btn-warning btn-market">Market XLM</button>
                  </td>
               </tr>
               <tr class="odd" role="row">
                  <td><strong><a href="">TEN</a></strong></td>
                  <td><a href="https://vip.bitcoin.co.id/finance/ten">Tokenomy</a></td>
                  <td>0 TEN</td>
                  <td class="text_right">0 IDR</td>
                  <td class="">
                     <button onclick="window.location='https://vip.bitcoin.co.id/finance/ten';" class="btn btn-xs btn-info btn-wd-depo">Deposit / Withdraw TEN</button>
                     <button onclick="window.location='https://vip.bitcoin.co.id/market/TENIDR';" class="btn btn-xs btn-warning btn-market">Market TEN</button>
                  </td>
               </tr>
               <tr class="even" role="row">
                  <td><strong><a href="">WAVES</a></strong></td>
                  <td><a href="https://vip.bitcoin.co.id/finance/waves">Waves</a></td>
                  <td>0 WAVES</td>
                  <td class="text_right">0 IDR</td>
                  <td class="">
                     <button onclick="window.location='https://vip.bitcoin.co.id/finance/waves';" class="btn btn-xs btn-info btn-wd-depo">Deposit / Withdraw WAVES</button>
                     <button onclick="window.location='https://vip.bitcoin.co.id/market/WAVESIDR';" class="btn btn-xs btn-warning btn-market">Market WAVES</button>
                  </td>
               </tr>
               <tr class="odd" role="row">
                  <td><strong><a href="">XRP</a></strong></td>
                  <td><a href="https://vip.bitcoin.co.id/finance/xrp">Ripple</a></td>
                  <td>0 XRP</td>
                  <td class="text_right">0 IDR</td>
                  <td class="">
                     <button onclick="window.location='https://vip.bitcoin.co.id/finance/xrp';" class="btn btn-xs btn-info btn-wd-depo">Deposit / Withdraw XRP</button>
                     <button onclick="window.location='https://vip.bitcoin.co.id/market/XRPIDR';" class="btn btn-xs btn-warning btn-market">Market XRP</button>
                  </td>
               </tr>
               <tr class="even" role="row">
                  <td><strong><a href="">XZC</a></strong></td>
                  <td><a href="https://vip.bitcoin.co.id/finance/xzc">ZCoin</a></td>
                  <td>0 XZC</td>
                  <td class="text_right">0 IDR</td>
                  <td class="">
                     <button onclick="window.location='https://vip.bitcoin.co.id/finance/xzc';" class="btn btn-xs btn-info btn-wd-depo">Deposit / Withdraw XZC</button>
                     <button onclick="window.location='https://vip.bitcoin.co.id/market/XZCIDR';" class="btn btn-xs btn-warning btn-market">Market XZC</button>
                  </td>
               </tr> -->
            </tbody>
            <tfoot>
               <tr class="odd">
                  <td colspan="3" class="text_right" rowspan="1">
                     <strong>Est. Asset Value</strong>
                  </td>
                  <td class="text_right" rowspan="1" colspan="1"><strong>0 IDR</strong></td>
                  <td rowspan="1" colspan="1"></td>
               </tr>
            </tfoot>
         </table>
      </div>
   </div>
</div>
@stop