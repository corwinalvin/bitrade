@extends('layouts.user-frontend.user-dashboard')
@section('content')
<div class="content">
<div class="row">
	<div class="col-md-12">
		<div class="spot-title-container">
			<h3>{{$title_market}} MARKET</h3>
			<h5>{{$market}}</h5>
		</div>
		<div class="table-responsive noborder">
			<table class="spot-stat-container">
				<tbody>
                <tr>
					<td class="market-price-box" style="background-color: rgb(255, 255, 255);">
						<strong class="price_{{$real_id}}_val"><img src="https://indodax.com/v2/images/loaders/loader8.gif" /></strong><br>
						<span class="text-muted">Last Price</span>
					</td>
					<td>
						<strong class="price_change_{{$real_id}} arrow-red" style="color: rgb(230, 57, 62);">&nbsp;<i class="fa fa-chevron-circle-down"></i><img src="https://indodax.com/v2/images/loaders/loader8.gif" /></strong><br>
						<span class="text-muted">24h Change</span>
					</td>
					<td>
						<strong class="low_val"><img src="https://indodax.com/v2/images/loaders/loader8.gif" /></strong><br>
						<span class="text-muted">Low</span>
					</td>
					<td>
						<strong class="high_val"><img src="https://indodax.com/v2/images/loaders/loader8.gif" /></strong><br>
						<span class="text-muted">High</span>
					</td>
				</tr>
				<tr>
					<td colspan="4" style="text-align: center;">
						<span class="text-muted">Volume 24h:</span>
						<strong class="vol_val"><img src="https://indodax.com/v2/images/loaders/loader8.gif" /></strong> <small>{{$from}}</small>
					</td>
				</tr>
			    </tbody>
            </table>
		</div>
		<div class="row">
			<div class="col-md-12 mb10" style="font-size: 18px;">
				Saldo: <strong><span class="balance_{{strtolower($from)}}_val">0,03415315</span> {{$from}}</strong> &nbsp;<button class="btn btn-primary btn-xs" onclick="window.location='{{route('detailFinance', $from)}}';">Deposit {{$from}} / Withdraw {{$from}}</button>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-lg-6">
     <h4>@lang('user-dashboard.buy-bitcoin', ['market' => ucwords(strtolower($title_market))])</h4>
        <form method="post" id="wizard-beli" class="panel-wizard">
        <ul class="nav nav-justified nav-wizard mt0 nav-disabled-click nav-pills">
            <li><a href="#tab1-beli" data-toggle="tab"><strong>Step 1:</strong> Input</a></li>
            <li><a href="#tab2-beli" data-toggle="tab" disable><strong>Step 2:</strong> @lang('user-dashboard.confirm')</a></li>
            <li><a href="#tab3-beli" data-toggle="tab"><strong>Step 3:</strong> @lang('user-dashboard.result')</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane" id="tab1-beli">
                <div class="form-group">
                    <label class="col-sm-4">@lang('user-dashboard.balance-rupiah'):</label>
                    <div class="col-sm-8">
                        <a href="#" class="balance_rp_val" onclick="$('#instan_rp_val').val(balance_rp).change(); return false;">{{$balance}}</a>
                        <span class="deposit_idr_link">
                            &nbsp;
                            [ <a href="{{route('deposit-fund')}}" class="underline">@lang('user-dashboard.click-deposit-rupiah')</a> ]
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 middle-label">@lang('user-dashboard.amount-in-rupiah'):</label>
                    <div class="col-sm-8">
                        <div class="input-group form-horizontal">
                            <input type="text" name="rp" class="form-control rp_formatter" placeholder="Jumlah Rupiah" id="instan_rp_val">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
                                <i class="fa fa-caret-left"></i>
                                </button>
                                <ul class="dropdown-menu amount-percent" role="menu">
                                    <li><a href="#" onclick="$('#instan_rp_val').val(balance_rp*0.1).change(); return false;">10%</a></li>
                                    <li><a href="#" onclick="$('#instan_rp_val').val(balance_rp*0.25).change(); return false;">25%</a></li>
                                    <li><a href="#" onclick="$('#instan_rp_val').val(balance_rp*0.5).change(); return false;">50%</a></li>
                                    <li><a href="#" onclick="$('#instan_rp_val').val(balance_rp*0.75).change(); return false;">75%</a></li>
                                    <li><a href="#" onclick="$('#instan_rp_val').val(balance_rp).change(); return false;">100%</a></li>
                                </ul>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">Harga 1 {{$from}}:</label>
                    <div class="col-sm-8">
                    Harga akan dikalkulasi sesuai dengan harga market pada saat order dipasang. Klik "Lanjutkan" untuk mendapatkan estimasi harga. </div>
                </div>
            </div>
            <div class="tab-pane" id="tab2-beli">
                <div class="form-group">
                    <div class="col-sm-12">
                    @lang('user-dashboard.desc-1') </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">@lang('user-dashboard.amount-in-rupiah'):</label>
                    <div class="col-sm-8" id="instan_buy_confirm_rp">
                        <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">@lang('user-dashboard.estimationofprice'):</label>
                    <div class="col-sm-8" id="instan_buy_confirm_harga">
                        <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">@lang('user-dashboard.fee'):</label>
                    <div class="col-sm-8" id="instan_buy_confirm_fee">
                        <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">@lang('user-dashboard.estimationofbought',['from' => $from]):</label>
                    <div class="col-sm-8" id="instan_buy_confirm_btc">
                        <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                    @lang('user-dashboard.desc-2',['from' => $from]) </div>
                </div>
            </div>
            <div class="tab-pane" id="tab3-beli">
                <div class="form-group hide" id="instan_buy_hasil_loading">
                    <div class="col-sm-12">
                        <center><img src="https://indodax.com/v2/images/loaders/loader8.gif" style="margin: 70px 0"></center>
                    </div>
                </div>
                <div id="instan_buy_hasil" class="hide form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-12">
                        Selamat, pembelian Bitcoin telah berhasil! Berikut adalah rinciannya. </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Rupiah Terpotong:</label>
                        <div class="col-sm-8" id="instan_buy_hasil_rp">
                            <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Bitcoin Didapatkan:</label>
                        <div class="col-sm-8" id="instan_buy_hasil_btc">
                            <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Biaya:</label>
                        <div class="col-sm-8" id="instan_buy_hasil_fee">
                            <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Harga Rata-Rata:</label>
                        <div class="col-sm-8" id="instan_buy_hasil_harga">
                            <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Waktu:</label>
                        <div class="col-sm-8" id="instan_buy_hasil_waktu">
                            <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <ul class="list-unstyled wizard">
            <li class="pull-left previous disabled hide"><button type="button" class="btn btn-default">Kembali</button></li>
            <li class="pull-right next"><button type="button" class="btn btn-primary">@lang('user-dashboard.continue')</button></li>
            <li class="pull-right finish hide"><button type="button" class="btn btn-primary" onclick="instan_buy_selesai(); return false;">Selesai</button></li>
        </ul>
    </form>
    </div>
    <div class="col-lg-6">
         <h4>@lang('user-dashboard.sell-bitcoin', ['market' => ucwords(strtolower($title_market))])</h4>
        <form method="post" id="wizard-beli" class="panel-wizard">
        <ul class="nav nav-justified nav-wizard mt0 nav-disabled-click nav-pills">
            <li><a href="#tab1-beli" data-toggle="tab"><strong>Step 1:</strong> Input</a></li>
            <li><a href="#tab2-beli" data-toggle="tab" disable><strong>Step 2:</strong> @lang('user-dashboard.confirm')</a></li>
            <li><a href="#tab3-beli" data-toggle="tab"><strong>Step 3:</strong> @lang('user-dashboard.result')</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane" id="tab1-beli">
                <div class="form-group">
                    <label class="col-sm-4">@lang('user-dashboard.balance-rupiah'):</label>
                    <div class="col-sm-8">
                        <a href="#" class="balance_rp_val" onclick="$('#instan_rp_val').val(balance_rp).change(); return false;">{{$balance}}</a>
                        <span class="deposit_idr_link">
                            &nbsp;
                            [ <a href="{{route('deposit-fund')}}" class="underline">@lang('user-dashboard.click-deposit-rupiah')</a> ]
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 middle-label">@lang('user-dashboard.amount-in-rupiah'):</label>
                    <div class="col-sm-8">
                        <div class="input-group form-horizontal">
                            <input type="text" name="rp" class="form-control rp_formatter" placeholder="Jumlah Rupiah" id="instan_rp_val">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
                                <i class="fa fa-caret-left"></i>
                                </button>
                                <ul class="dropdown-menu amount-percent" role="menu">
                                    <li><a href="#" onclick="$('#instan_rp_val').val(balance_rp*0.1).change(); return false;">10%</a></li>
                                    <li><a href="#" onclick="$('#instan_rp_val').val(balance_rp*0.25).change(); return false;">25%</a></li>
                                    <li><a href="#" onclick="$('#instan_rp_val').val(balance_rp*0.5).change(); return false;">50%</a></li>
                                    <li><a href="#" onclick="$('#instan_rp_val').val(balance_rp*0.75).change(); return false;">75%</a></li>
                                    <li><a href="#" onclick="$('#instan_rp_val').val(balance_rp).change(); return false;">100%</a></li>
                                </ul>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">Harga 1 BTC:</label>
                    <div class="col-sm-8">
                    Harga akan dikalkulasi sesuai dengan harga market pada saat order dipasang. Klik "Lanjutkan" untuk mendapatkan estimasi harga. </div>
                </div>
            </div>
            <div class="tab-pane" id="tab2-beli">
                <div class="form-group">
                    <div class="col-sm-12">
                    @lang('user-dashboard.desc-1') </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">@lang('user-dashboard.amount-in-rupiah'):</label>
                    <div class="col-sm-8" id="instan_buy_confirm_rp">
                        <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">@lang('user-dashboard.estimationofprice'):</label>
                    <div class="col-sm-8" id="instan_buy_confirm_harga">
                        <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">@lang('user-dashboard.fee'):</label>
                    <div class="col-sm-8" id="instan_buy_confirm_fee">
                        <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">@lang('user-dashboard.estimationofbought', ['from' => $from]):</label>
                    <div class="col-sm-8" id="instan_buy_confirm_btc">
                        <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                    @lang('user-dashboard.desc-2',['from' => $from]) </div>
                </div>
            </div>
            <div class="tab-pane" id="tab3-beli">
                <div class="form-group hide" id="instan_buy_hasil_loading">
                    <div class="col-sm-12">
                        <center><img src="https://indodax.com/v2/images/loaders/loader8.gif" style="margin: 70px 0"></center>
                    </div>
                </div>
                <div id="instan_buy_hasil" class="hide form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-12">
                        Selamat, pembelian Bitcoin telah berhasil! Berikut adalah rinciannya. </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Rupiah Terpotong:</label>
                        <div class="col-sm-8" id="instan_buy_hasil_rp">
                            <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Bitcoin Didapatkan:</label>
                        <div class="col-sm-8" id="instan_buy_hasil_btc">
                            <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Biaya:</label>
                        <div class="col-sm-8" id="instan_buy_hasil_fee">
                            <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Harga Rata-Rata:</label>
                        <div class="col-sm-8" id="instan_buy_hasil_harga">
                            <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4">Waktu:</label>
                        <div class="col-sm-8" id="instan_buy_hasil_waktu">
                            <img src="https://indodax.com/v2/images/loaders/loader8.gif">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <ul class="list-unstyled wizard">
            <li class="pull-left previous disabled hide"><button type="button" class="btn btn-default">Kembali</button></li>
            <li class="pull-right next"><button type="button" class="btn btn-primary">@lang('user-dashboard.continue')</button></li>
            <li class="pull-right finish hide"><button type="button" class="btn btn-primary" onclick="instan_buy_selesai(); return false;">Selesai</button></li>
        </ul>
    </form>
    </div>
</div>
    <div class="row">
        <div class="col-lg-6">
            <h5 style="font-weight:bold;">Market Jual</h5>
            <div class="panel panel-default">
                <div class="table-responsive spot-data-table" style="height: 390px; overflow: auto;">
                    <table class="table table-primary mb30 table-bordered order_table" id="sell_orders">
                        <thead>
                            <tr>
                                <th>Harga</th>
                                <th style="width: 33%;">{{$from}}</th>
                                <th style="width: 33%;">{{$to}}</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <h5 style="font-weight:bold;">Market Beli</h5>
            <div class="panel panel-default">
                <div class="table-responsive spot-data-table" style="height: 390px; overflow: auto;">
                    <table class="table table-primary mb30 table-bordered order_table" id="sell_orders">
                        <thead>
                            <tr>
                                <th>Harga</th>
                                <th style="width: 33%;">{{$from}}</th>
                                <th style="width: 33%;">{{$to}}</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
	
    var pusher = new Pusher('a0dfa181b1248b929b11', {
      cluster: 'ap1',
      encrypted: true
    });

	var update_tradedata = function(data, first_load){
		update_price_all(data.prices);
		update_volumes(data.volumes);
		update_price_arrow(data.prices, data.prices_24h);
		last_price = data.last_price;
		high_price = data._24h.high;
		low_price = data._24h.low;
		vol_{{strtolower($from)}} = data._24h.vol_{{strtolower($from)}}/1e8;
		vol_rp = data._24h.vol_rp;
		var buy_orders = data.buy_orders;
		var sell_orders = data.sell_orders;
		if(data.buy_orders.length>0) bid_price = data.buy_orders[0].price;
		else bid_price = 0;
		if(data.sell_orders.length>0) ask_price = data.sell_orders[0].price;
		else ask_price = 0;
		
		var old_title = $('title').html();
		var new_title = '('+rp_format(last_price)+' {{$to}}/{{$from}}) '+old_title.replace(/\(.+\)/, '');
		$('title').html(new_title);
		
		$('.last_price_val').html($.number(data.prices.{{$real_id}}, 0, decpoint, thousep));
		$('.low_val').html($.number(data._24h.low, 0, decpoint, thousep));
		$('.high_val').html($.number(data._24h.high, 0, decpoint, thousep));
		$('.vol_val').html($.number(vol_{{strtolower($from)}}, vol_{{strtolower($from)}}>1?1:2, decpoint, thousep)).attr('title', rp_format(vol_rp)+' IDR');
		$('#sell_orders tbody').html(table_sell_orders(data.sell_orders));
		$('#buy_orders tbody').html(table_buy_orders(data.buy_orders));
		// $('#last_trades tbody').html(table_last_trades(data.last_trades));
		
		// if($('.js-marquee').length) $('.js-marquee').html(data.marquee);
		// else{ 
		// 	$('.marquee').html(data.marquee);
			// $('.marquee').marquee({
			// 	duration: 15000,
			// 	gap: 0,
			// 	duplicated: true,
			// 	pauseOnHover: false
			// });
		// }
		
		if(first_load){
			$('#buy_price').val((ask_price*1)-1000).change();
			$('#sell_price').val((bid_price*1)+1000).change();
		}
		
		$('.order_table tbody tr td').click(function(){
			var price = $(this).parent('tr').attr('price');
			$('#buy_price, #sell_price').val(price).change();
		});
	}
	
	var pusher_tradedata = false;
	var start_pusher_tradedata = function(){
		if(pusher_tradedata) return;
		pusher_tradedata = pusher.subscribe('tradedata-{{$real_id}}');
		pusher_tradedata.bind('update', function(data) {
			update_tradedata(data);
		});
		// pusher_tradedata.bind('update_chart', function(data) {
		// 	update_chartdata(data);
		// });
	}
	
	var stop_pusher_tradedata = function(){
		if(!pusher_tradedata) return;
		pusher_tradedata.unbind('update');
		pusher_tradedata.unbind('update_chart');
		pusher_tradedata.unsubscribe('tradedata-{{$real_id}}');
		pusher_tradedata = false;
	}

	// update_chartdata = function(data){
	// 	var chart = $('#chart{{strtolower($from)}}').highcharts();
	// 	if(!data.chart.length){
	// 		return;
	// 	}
			
	// 	var ohlc = [], volume = [], dataLength = data.chart.length;
	// 	var chartData = data.chart;
	// 	i = 0;
	// 	for (i; i < dataLength; i += 1) {
	// 		ohlc.push([chartData[i][0],chartData[i][1],chartData[i][2],chartData[i][3],chartData[i][4]]);
	// 		volume.push([chartData[i][0],chartData[i][5]]);
	// 	}
	// 	chart.series[1].setData(volume,false);
	// 	chart.series[0].setData(ohlc,false);
	// 	chart.hideLoading();
		
	// 	chart.redraw();
	// 	chart.xAxis[0].setExtremes(Date.now()-86400*1000, Date.now());
	// }

    
    $(function(){
        start_pusher_tradedata();
        
        $(document).idle({
            onHide: function(){
                stop_pusher_tradedata();
            },
            onShow: function(){
                start_pusher_tradedata();
            }
        });
    });

    if (typeof up_to_date === "function") { 
        setInterval(up_to_date, 60000);
    }

    table_sell_orders = function(sell_orders){
			if(sell_orders.length<1){
				return '<tr>\
					<td colspan="3"><center>-kosong-</center></td>\
				</tr>';
			}
			var html = '';
			var total_coin = 0;
			var total_rp = 0;
			
			$.each(sell_orders, function(index,sell_order){
				var price = sell_order.price*1;
				var sum_coin = sell_order.sum_{{strtolower($from)}}/1e8;
				var sum_rp = sell_order.price*sum_coin;
				total_coin += sum_coin;
				total_rp += sum_rp;
				if(sum_rp>50000000) var is_bold = true;
				else var is_bold = false;
				html += '<tr price="'+sell_order.price+'" title="Total: '+btc_format(total_coin)+' {{$from}} / '+rp_format(total_rp)+' IDR">\
					<td'+(is_bold?' style="font-weight: bold; background-color: #FFFFC6;"':'')+'>'+rp_format(price)+'</td>\
					<td'+(is_bold?' style="font-weight: bold; background-color: #FFFFC6;"':'')+'>'+btc_format(sum_coin)+'</td>\
					<td'+(is_bold?' style="font-weight: bold; background-color: #FFFFC6;"':'')+'>'+rp_format(sum_rp)+'</td>\
				</tr>';
			});
			return html;
		}
	
	table_buy_orders = function(buy_orders){
		if(buy_orders.length<1){
			return '<tr>\
				<td colspan="3"><center>-kosong-</center></td>\
			</tr>';
		}
		var html = '';
		var total_coin = 0;
		var total_rp = 0;
		var fee = 0.3/100;
		
		$.each(buy_orders, function(index,buy_order){
			var price = buy_order.price*1;
			var sum_rp = buy_order.sum_rp*1;
			var sum_coin = buy_order.sum_rp/price;
			total_coin += sum_coin;
			total_rp += sum_rp;
			if(sum_rp>50000000) var is_bold = true;
			else var is_bold = false;
			html += '<tr price="'+buy_order.price+'" title="Total: '+btc_format(total_coin)+' {{$from}} / '+rp_format(total_rp)+' IDR">\
				<td'+(is_bold?' style="font-weight: bold; background-color: #FFFFC6;"':'')+'>'+rp_format(price)+'</td>\
				<td'+(is_bold?' style="font-weight: bold; background-color: #FFFFC6;"':'')+'>'+btc_format(sum_coin)+'</td>\
				<td'+(is_bold?' style="font-weight: bold; background-color: #FFFFC6;"':'')+'>'+rp_format(sum_rp)+'</td>\
			</tr>';
		});
		return html;
	}

$(function(){
	$('.table-markets-large tr').click(function(){
		if($(this).attr('href')) window.location = $(this).attr('href');
	});
});

jQuery('#wizard-beli').bootstrapWizard({
    onTabShow: function(tab, navigation, index) {
        tab.prevAll().addClass('done');
        tab.nextAll().removeClass('done');
        tab.removeClass('done');
                    
        var $total = navigation.find('li').length;
        var $current = index + 1;
        
        var instan_rp_val = ($('#instan_rp_val').val()+'').replace(/[^0-9]/g,'');
        
        if($current==1){ 
            $('#wizard-beli').find('.wizard .previous').addClass('hide');
            $('#wizard-beli').find('.wizard .next').removeClass('hide');
            $('#wizard-beli').find('.wizard .finish').addClass('hide');
        }
        
        else if($current==2){ //konfirmasi
            $('#wizard-beli').find('.wizard .previous').removeClass('hide');
            $('#wizard-beli').find('.wizard .next').addClass('hide');
            $('#wizard-beli').find('.wizard .finish').addClass('hide');
            
            $('#instan_buy_confirm_rp, #instan_buy_confirm_harga, #instan_buy_confirm_btc').html('<img src="https://indodax.com/v2/images/loaders/loader8.gif" />');
            
            $('#instan_buy_confirm_rp').html(rp_format(instan_rp_val));
                @if($from == "acn")
                var url = API_BASE+'api/{{$real_id}}/depth';
                @else
                var url = 'https://activacoin.com/api/price/gettrade';
                @endif
            $.ajax({
                url: url,
                dataType: 'json',
                type: 'get'
            })
            .done(function(data) {
                
                var rp = instan_rp_val;
                
                buy_fee = Math.floor(rp-(rp/(1+trade_fee)));
                var rp_remain = rp-buy_fee;
                var btc_got = 0;
                var estimation_last_price = false;
                @if($from == "acn")
                var sell_orders = data.sell;
                @else
                var sell_orders = data.response.buy;
                @endif
                var sum_rp = 0;
                var sum_btc = 0;
                
                var index;
                for (index = 0; index < sell_orders.length; ++index) {
                    sum_btc = sell_orders[index].ask_acn*1;
                    sum_rp = sell_orders[index].ask_acn*sell_orders[index].bid_vloume;
                    if(rp_remain>sum_rp){
                        rp_remain -= sum_rp;
                        btc_got += sum_btc;
                    }
                    else{
                        btc_got += rp_remain/sell_orders[index].bid_vloume;
                        estimation_last_price = sell_orders[index].bid_vloume;
                        rp_remain = 0;
                        break;
                    }
                }
                
                // if(!parseInt(rp_remain)){ 
                    btc_got = (Math.floor(btc_got*100000000)/100000000)+'';
                    $('#instan_buy_confirm_btc').html(btc_got.replace(thousep, decpoint)+' {{$from}}');
                    $('#instan_buy_confirm_harga').html(rp_format((rp-buy_fee)/btc_got));
                    $('#instan_buy_confirm_fee').html(rp_format(buy_fee));
                    $('#wizard-beli').find('.wizard .next').removeClass('hide');
                    
                    buy_confirm_timeout = Math.floor(Date.now() / 1000)+60;
                    
                    return false;
                // }
                // else{ 
                //     console.log(buy_fee);
                //     console.log(rp);
                //     console.log(parseInt(rp_remain));
                //     console.log(sum_rp);
                //     alert_error('Transaksi terlalu besar untuk metode instan. Silakan gunakan Marketplace.');
                //     $('#wizard-beli').bootstrapWizard('previous');
                //     return false;
                // }
                
            })
            .fail(function() {
                alert_error( "Server tidak merespon. Mohon cek koneksi internet anda.\nServer not responding. Please check your internet connection." );
            })
            .always(function() {
            });
            
        }
        
        else if($current==3){ //hasil
            $('#wizard-beli').find('.wizard .previous').addClass('hide');
            $('#wizard-beli').find('.wizard .next').addClass('hide');
            
            $('#instan_buy_hasil_loading').removeClass('hide');
            $('#instan_buy_hasil').addClass('hide');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '{{route("market.buy")}}',
                data: {
                    pair: '{{$market}}',
                    buy_amount_rp: instan_rp_val
                },
                dataType: 'json',
                type: 'post'
            })
            .done(function(data) {
                if(data.is_error){ 
                    alert_error(data.error);
                    $('#wizard-beli').bootstrapWizard('previous');
                    return;
                }
                
                $('#instan_buy_hasil_rp').html(rp_format(data.total_trade_rp+data.total_fee_rp));
                $('#instan_buy_hasil_btc').html(btc_format(data.total_trade_btc/1e8)+' {{$from}}');
                $('#instan_buy_hasil_fee').html(rp_format(data.total_fee_rp));
                $('#instan_buy_hasil_harga').html(rp_format(data.total_trade_rp/data.total_trade_btc*1e8));
                $('#instan_buy_hasil_waktu').html(data.time);
                
                $('#instan_buy_hasil_loading').addClass('hide');
                $('#instan_buy_hasil').removeClass('hide');
                $('#wizard-beli').find('.wizard .finish').removeClass('hide');
                
                up_to_date();
            })
            .fail(function() {
                alert_error( "Server tidak merespon. Mohon cek koneksi internet anda.\nServer not responding. Please check your internet connection." );
            })
            .always(function() {
            });            
        }
    },
    onNext: function(tab, navigation, index){
        var $current = index + 1;
        var instan_rp_val = ($('#instan_rp_val').val()+'').replace(/[^0-9]/g,'')*1;
        if($current>=2){
            if(instan_rp_val<trade_rp_min){
                alert_error('Minimal pembelian adalah Rp. '+rp_format(trade_rp_min));
                return false;
            }
            if(instan_rp_val>balance_rp){
                alert_error('Saldo tidak mencukupi.');
                return false;
            }
        }
        if($current==3){
            if(buy_confirm_timeout<Math.floor(Date.now() / 1000)){
                alert_error('Order telah kadaluarsa (melewati 60 detik). Silakan ulangi dari step 1.');
                $('#wizard-beli').bootstrapWizard('previous');
                return false;
            }
        }
        
    },
    onTabClick: function(tab, navigation, index) {
        return false;
    }
});
</script>
@endsection