@extends('layouts.user-frontend.user-dashboard')
@section('content')
<div class="content">
  <h2>@lang('send-receive.balance'): <strong>{{$balance}} {{$page_title}}</strong></h2>
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">@lang('send-receive.receive', ['market' => $market])</a></li>
    <li><a data-toggle="tab" href="#menu1">@lang('send-receive.send',['market' => $market])</a></li>
    <li><a data-toggle="tab" href="#menu2">@lang('send-receive.history')</a></li>
  </ul>
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <div class="row mt10">
        <div class="col-md-6">
          <div class="form-group">
            <label for="">@lang('send-receive.desc-send', ['market' => $market]):</label>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" style="padding: 0">@lang('send-receive.bitcoin-address', ['market' => $market]):</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" style="width: 100%;" value="{{$address}}" readonly="readonly" style="cursor: text;" onclick="if(!navigator.userAgent.match(/(mobi|android)/i)) $(this).select();" id="coin_address"/>
            </div>
          </div>
          <div style="margin-top: 70px;">
            <ul style="font-size:12px; padding-left:13px;display: ">
              <li>@lang('send-receive.first_desc')</li>
              <li>@lang('send-receive.sec_desc')</li>
              <li>@lang('send-receive.thir_desc', ['market' => $market])</li>
              <li>@lang('send-receive.fourth_desc')</li>
            </ul>
          </div>
        </div>
      <div class="col-md-6">
        <div class="row">
          <div class="col-md-8">
            <p>@lang('send-receive.scan-qr-desc', ['market' => $market])</p>
          </div>
          <div class="col-md-4">
            <img src="https://chart.googleapis.com/chart?cht=qr&amp;chld=Q|2&amp;chs=300&amp;chl={{$address}" style="width: 100%; margin-top: -10px;">
          </div>
        </div>
      </div>
      </div>
    </div>
    <div id="menu1" class="tab-pane fade">
      <div class="row mt10">
        <div class="col-md-6">
          <div class="form-group">
            <label class="col-sm-12 control-label nopadding">@lang('send-receive.fill-desc', ['market' => $market])</label>
          </div>
          <form action="" class="form-horizontal">
            <div class="form-group">
              <label class="col-sm-4 control-label nopadding mt10">@lang('send-receive.amount') BTC:</label>
              <div class="col-sm-8">
                <div class="input-group">
                  <input type="text" name="withdraw_amount_btc" class="form-control btc_formatter" placeholder="Jumlah Pengiriman Sebelum Biaya" id="withdraw_amount_btc">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
                    <i class="fa fa-caret-left"></i>
                    </button>
                    <ul class="dropdown-menu amount-percent" role="menu">
                      <li><a href="#" onclick="$('#withdraw_amount_btc').val(btc_format(balance_btc*0.1)).change(); return false;">10%</a></li>
                      <li><a href="#" onclick="$('#withdraw_amount_btc').val(btc_format(balance_btc*0.25)).change(); return false;">25%</a></li>
                      <li><a href="#" onclick="$('#withdraw_amount_btc').val(btc_format(balance_btc*0.5)).change(); return false;">50%</a></li>
                      <li><a href="#" onclick="$('#withdraw_amount_btc').val(btc_format(balance_btc*0.75)).change(); return false;">75%</a></li>
                      <li><a href="#" onclick="$('#withdraw_amount_btc').val(btc_format(balance_btc)).change(); return false;">100%</a></li>
                    </ul>
                  </span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label nopadding mt10">@lang('send-receive.sms-fee'):</label>
              <label class="col-sm-8 control-label mt10" style="text-align: left;">
                <b style="color: #E10000;">0,0001 BTC</b>&nbsp; (@lang('send-receive.remove-sms'), <a href="#" style="text-decoration: underline;" onclick="window.location=ROOT+'enable_gauth';return false;">klik sini</a>)
              </label>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label nopadding mt10">@lang('send-receive.withdraw-fee'):</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" readonly="readonly" value="0,0005" id="withdraw_fee">
                <div class="mt10" id="internal_desc" style="display: none; color: #4a535e;"><b style="color: #E10000;">0</b>&nbsp; (<a href="https://help.bitcoin.co.id/do-i-have-to-pay-for-any-fees-when-i-am-moving-bitcoin-from-my-vip-account-to-other-vip-accounts/" target="_blank" style="border-bottom: 1px #428bca dotted;">transaksi internal</a>)</div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label nopadding mt10">@lang('send-receive.you-receive'):</label>
              <div class="col-sm-8">
                <!-- Jumlah Bersih BTC yang akan Diterima -->
                <input type="text" class="form-control" placeholder="@lang('send-receive.you-receive')" id="withdraw_btc_net">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label nopadding mt10">@lang('send-receive.bitcoin-address'):</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="@lang('send-receive.bitcoin-address')" id="withdraw_btc_address" name="withdraw_btc_address" value="">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label nopadding mt10">@lang('send-receive.pin-sms'):</label>
              <div class="col-sm-8">
                <div class="row">
                  <div class="col-sm-6">
                    <input type="text" class="form-control sms-pin" style="width:100%;" placeholder="@lang('send-receive.pin-sms')" id="withdraw_sms_pin">
                  </div>
                  <div class="col-sm-6" >
                    <button class="btn btn-danger btn-xs mt5 btn-send-sms-pin">@lang('send-receive.send-pin-sms')</button>
                  </div>
                  <div class="col-sm-12 mt10 small-note">
                    @lang('send-receive.sms-note.1') @lang('send-receive.sms-note.2')
                  </div>
                </div>
              </div>
            </div>
          </form>
          <div class="form-group">
            <div class="col-sm-12 text-center nopadding mt20 mb20">
              <button class="btn btn-warning btn-width-100-percent form-control" onclick="withdraw_coin();">@lang('send-receive.send',['market' => $market])
              </button>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="alert alert-warning">
            <strong>Warning!</strong> @lang('send-receive.check-bitcoin-address', ['market' => $market])
          </div>
          <ul class="list-desc">
            <li>@lang('send-receive.first_req')</li>
            <li>@lang('send-receive.sec_req.1')
              <a href="https://vip.bitcoin.co.id/verify" style="text-decoration: underline;">@lang('send-receive.sec_req.2')</a> @lang('send-receive.sec_req.3')).
            </li>
            <li>@lang('send-receive.third_req')</li>
            <li>@lang('send-receive.four_req')</li>
            <li>@lang('send-receive.fifth_req', ['market' => $market])</li>
            <li>@lang('send-receive.six_req')</li>
            <li>@lang('send-receive.sev_req', ['market' => $market])</li>
          </ul>
        </div>
      </div>
    </div>
    <div id="menu2" class="tab-pane fade">
      <div class="form-group">
        <label class="col-sm-12 control-label nopadding">@lang('send-receive.history-view')</label>
      </div>
      <div class="col-lg-6">
        <form action="" class="form-horizontal">
          <div class="form-group">
            <label for="" class="col-sm-4">@lang('send-receive.month')</label>
            <div class="col-sm-8">
              <select name="month" data-placeholder="Pilih Bulan" style="width: 100%" id="history_filter_month" tabindex="-1" title="" class="select2-offscreen">
                <option value=""></option>
                <option value="1">Januari</option>
                <option value="2" selected="selected">Februari</option>
                <option value="3">Maret</option>
                <option value="4">April</option>
                <option value="5">Mei</option>
                <option value="6">Juni</option>
                <option value="7">Juli</option>
                <option value="8">Agustus</option>
                <option value="9">September</option>
                <option value="10">Oktober</option>
                <option value="11">November</option>
                <option value="12">Desember</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-4">@lang('send-receive.year')</label>
            <div class="col-sm-8">
              <select name="month" data-placeholder="Pilih Tahun" style="width: 100%" id="history_filter_year" tabindex="-1" title="" class="select2-offscreen">
                <option value=""></option>
                <option value="1">2014</option>
                <option value="2">2015</option>
                <option value="3">2016</option>
                <option value="3">2017</option>
                <option value="3">2018</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="" class="col-sm-4"></label>
            <div class="col-sm-8">
              <button type="submit" class="btn btn-info form-control">@lang('send-receive.view-history')</button>
            </div>
          </div>
        </form>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <h5><strong>@lang('send-receive.history-desc', ['market' => $market]) FEBRUARI 2018</strong></h5>
          <div class="table-responsive">
            <table class="table table-bordered table-striped mb30">
              <thead>
                <tr>
                  <th width="20%">@lang('send-receive.time')</th>
                  <th width="10%">@lang('send-receive.type')</th>
                  <th width="10%">@lang('send-receive.amount')</th>
                  <th width="20%">@lang('send-receive.destination')</th>
                  <th width="20%">TX</th>
                  <th width="20%">Status</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td colspan="6"><center>-@lang('send-receive.empty')-</center></td>
                </tr>
              </tbody></table>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="https://vip.bitcoin.co.id/v2/js/select2.min.js"></script>
<script>
$(document).ready(function() {
    $("#history_filter_month").select2({
  minimumResultsForSearch: -1
});
  $("#history_filter_year").select2({
    minimumResultsForSearch: -1
  });
});
</script>
@stop