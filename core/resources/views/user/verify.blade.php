@extends('layouts.user-frontend.user-dashboard')
@section('css')
<link href="https://vip.bitcoin.co.id/v2/css/select2.css" rel="stylesheet" />
<style>
.form-control{
	width:100% !important;
}
</style>
@endsection
@section('content')
<div class="content">
	<h3>@lang('verification.title')</h3>
	<p>@lang('verification.desc-1')</p>
	<p>@lang('verification.desc-2')</p>
	<ul>
		<li>@lang('verification.desc-3')</li>
		<li>@lang('verification.desc-4')</li>
	</ul> 
	<p>@lang('verification.desc-6')</p>
	<ul>
		<li>@lang('verification.req-1')</li>
		<li>@lang('verification.req-2')</li>
		<li>@lang('verification.req-3')</li>
		<li>@lang('verification.req-4')</li>
		<li>@lang('verification.req-5')</li>
		<li>@lang('verification.req-6')</li>
	</ul>
	<p>@lang('verification.desc-7')</p>
	<div class="row">
		<div class="col-md-12">
			
		</div>
	</div>
	<div class="row">
		
		<div class="col-md-6 col-lg-8">
			</div>
	</div>
</div>
<div class="mt20" id="form_verifikasi">
	<div class="col-md-12">
		<h3 style="margin-top: 10px;">@lang('verification.formulir-title')</h3>
		<p>@lang('verification.desc-8')</p>
	</div>
</div>
<div class="content">
	<div class="col-md-8 col-md-6">
		@if($errors->any())
		@foreach ($errors->all() as $error)
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			{!!  $error !!}
		</div>
		@endforeach
		@endif
		<form action="{{route('verify')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
			{{csrf_field()}}
			<div class="form-group">
				<label class="col-sm-4 control-label nopadding mt10">@lang('verification.name'):</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" readonly="readonly" value="{{Auth::user()->name}}" id="name" name="name">
					<i style="font-size: 10px;"><a href="" style="text-decoration: underline;" onclick="ganti_nama(); return false;" id="ganti_nama_link">@lang('verification.name-change')</a></i>
				</div>
			</div>
			<div class="form-group ganti_nama_container" style="display: none;">
				<label class="col-sm-4 control-label nopadding mt10">@lang('verification.new-name'):</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" value="" id="nama_baru" name="nama_baru">
					<i style="font-size: 10px;"><a href="" style="text-decoration: underline;" onclick="batal_ganti_nama(); return false;">@lang('verification.cancel-name')</a></i>
				</div>
			</div>
			<div class="form-group ganti_nama_container" style="display: none;">
				<label class="col-sm-4 control-label nopadding mt10">@lang('verification.reason-new'):</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" value="" id="alasan_ganti_nama" name="alasan_ganti_nama">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label nopadding mt10">@lang('verification.ktp-sim'):</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" value="{{Auth::User()->no_ktp}}" id="nik" name="no_ktp">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label nopadding mt10">@lang('verification.place-birth'):</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" value="{{Auth::User()->place_birth}}" id="tempat_lahir" name="place_birth">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label nopadding mt10">@lang('verification.date-birth'):</label>
				<div class="col-sm-8">
					<input type="text" class="form-control hasDatepicker" value="{{Auth::User()->date_birth}}" id="tanggal_lahir" name="date_birth">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label nopadding mt10">@lang('verification.gender'):</label>
				<div class="col-sm-8">
					<select name="sex" id="gender" style="width: 100%;" data-placeholder="pilih" tabindex="-1" title="" class="select2-offscreen">
						<option value=""></option>
						<option value="m" {{(Auth::user()->sex=="m") ? 'selected=selected':''}}>@lang('verification.boy')</option>
						<option value="f" {{(Auth::user()->sex=="f") ? 'selected=selected':''}}>@lang('verification.girl')</option>
					</select> </div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label nopadding mt10">@lang('verification.nationality'):</label>
					<div class="col-sm-8">
						<select name="nationality" id="warga_negara" style="width: 100%;" data-placeholder="pilih" tabindex="-1" title="" class="select2-offscreen">
							@php
							$countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
							@endphp
							<option value=""></option>
							@foreach($countries as $country)
							<option value="{{$country}}" {{(Auth::User()->nationality==$country) ? 'selected=selected' : ''}}>{{$country}}</option>
							@endforeach
						</select> </div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label nopadding mt10">@lang('verification.file-ktp-sim'):</label>
						<div class="col-sm-8">
							<input type="file" name="ktp_image" class="form-control">
							<i style="font-size: 10px; float: left; margin-top: 2px;">@lang('verification.maks-file')</i>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label nopadding mt10">@lang('verification.above-photo')</label>
						<div class="col-sm-8">
							<input type="file" name="verify_photo" class="form-control">
							<i style="font-size: 10px; float: left; margin-top: 2px;">@lang('verification.maks-file')</i>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12 text-center nopadding mt20 mb20">
							<button class="btn btn-warning btn-width-100-percent form-control" type="submit">@lang('verification.send')</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="modal fade modal-ganti-nama" tabindex="-1" role="dialog" data-backdrop="static">
			<div class="modal-dialog modal-md">
				<div class="modal-content">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h4 class="panel-title">@lang('verification.change-account')</h4>
						</div>
						<div class="panel-body">
							<p>@lang('verification.desc-9')</p>
							<div class="input-group mb15">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
								<input type="text" class="form-control" placeholder="@lang('verification.new-name')" name="nama_baru_pop" id="nama_baru_pop">
							</div>
							<div class="input-group mb15">
								<span class="input-group-addon"><i class="fa fa-commenting-o"></i></span>
								<input type="text" class="form-control" placeholder="@lang('verification.reason-new')" name="alasan_ganti_nama_pop" id="alasan_ganti_nama_pop">
							</div>
							<p>@lang('verification.allowed-title'):</p>
							<ul class="list-desc">
								<li>@lang('verification.st-7')</li>
								<li>@lang('verification.st-8')</li>
								<li>@lang('verification.st-9')</li>
							</ul>
							<p>@lang('verification.desc-10').</p>
						</div>
						<div class="panel-footer">
							<div>
								<button class="btn btn-default" style="text-align:left;" data-dismiss="modal"><i class="fa fa-times"></i> @lang('verification.back')</button>
								<button class="btn btn-warning pull-right" style="text-align:right;" onclick="ganti_nama_submit(); return false;"><i class="fa fa-check"></i> @lang('verification.submit')</button>
							</div>
							<!-- <div class="pull-right">
							</div> -->
						</div>
					</div>
				</div>
			</div>
		</div>
</div>
@stop
@section('scripts')
<script src="https://vip.bitcoin.co.id/v2/js/select2.min.js"></script>
<script>
$(document).ready(function() {
$("#gender").select2({
minimumResultsForSearch: -1
});
$("#warga_negara").select2({
minimumResultsForSearch: -1
});
});
var ganti_nama = function(){
		$('input[name="nama_baru"], input[name="alasan_ganti_nama"], #nama_baru_pop, #alasan_ganti_nama_pop').val('');
		$( ".modal-ganti-nama").modal('show');
	}
		var ganti_nama_submit = function(){
		var nama_baru_pop = $('#nama_baru_pop').val();
		var alasan_ganti_nama_pop = $('#alasan_ganti_nama_pop').val();
		
		if(nama_baru_pop.trim().length<1||alasan_ganti_nama_pop.trim().length<1){
			alert('Mohon lengkapi semua isian yang ada.');
			return false;
		}
		else{
			$('input[name="nama_baru"]').val(nama_baru_pop);
			$('input[name="alasan_ganti_nama"]').val(alasan_ganti_nama_pop);
			$('.ganti_nama_container').show();
			$('#ganti_nama_link').hide();
			$( ".modal-ganti-nama" ).modal('hide');
		}
		
	}
	
	var batal_ganti_nama = function(){
		$('input[name="nama_baru"], input[name="alasan_ganti_nama"], #nama_baru_pop, #alasan_ganti_nama_pop').val('');
		$('.ganti_nama_container').hide();
		$('#ganti_nama_link').show();
	}
</script>
@stop