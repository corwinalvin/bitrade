@extends('layouts.user-frontend.user-dashboard')

@section('style')
    <style>
        input[type="tel"],
        input[type="url"],
        input[type="password"] {
            width: 100%;
        }
    </style>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">

                <div class="portlet-body form">
                    <form class="form-horizontal" action="" method="post" role="form">
                        <div class="form-body">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label class="col-md-3 control-label"><strong>@lang('register.current-password')</strong></label>

                                <div class="col-md-6">
                                    <input class="form-control input-lg" name="current_password"
                                           placeholder="@lang('register.current-password')" type="password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><strong>@lang('register.new-password')</strong></label>

                                <div class="col-md-6">
                                    <input class="form-control input-lg" name="password" placeholder="@lang('register.new-password')"
                                           type="password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><strong>@lang('register.new-password-again')</strong></label>

                                <div class="col-md-6">
                                    <input class="form-control input-lg" name="password_confirmation"
                                           placeholder="@lang('register.new-password-again')" type="password">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                    <button type="submit" class="btn blue btn-block btn-lg bold">@lang('register.change-password')</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!---ROW-->

@endsection

@section('script')
    @if (session('message'))

        <script type="text/javascript">

            $(document).ready(function(){

                swal("Success!", "{{ session('message') }}", "success");

            });

        </script>

    @endif



    @if (session('alert'))

        <script type="text/javascript">

            $(document).ready(function(){

                swal("Sorry!", "{{ session('alert') }}", "error");

            });

        </script>

    @endif
@endsection
