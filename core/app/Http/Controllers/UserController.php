<?php

namespace App\Http\Controllers;

use App\BasicSetting;
use App\Compound;
use App\Deposit;
use App\DepositImage;
use App\Investment;
use App\PaymentLog;
use App\PaymentMethod;
use App\Plan;
use App\Repeat;
use App\RepeatLog;
use App\Support;
use App\SupportMessage;
use App\TraitsFolder\MailTrait;
use App\User;
use App\UserLog;
use App\WithdrawLog;
use App\WithdrawMethod;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Denpa\Bitcoin\Client as BitcoinClient;
use GuzzleHttp\Client;
use DB;

class UserController extends Controller
{
    use MailTrait;
    
    public function __construct()
    {
        $this->middleware('verifyUser');
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $data['user'] = User::findOrFail(Auth::user()->id)->balance;
            view()->share('balance', $data['user']);

            return $next($request);
        });
        session(['active_lang' => 'id']);
    }
    public function getDashboard()
    {

        $data['page_title'] = 'User Dashboard';

        $data['reference_title'] = "Reference User";
        $data['basic_setting'] = BasicSetting::first();
        $data['reference_user'] = User::whereUnder_reference(Auth::user()->id)->orderBy('id','desc')->get();

        $data['user'] = User::findOrFail(Auth::user()->id);
        $data['deposit'] = Deposit::whereUser_id(Auth::user()->id)->whereStatus(1)->sum('amount');
        $data['repeat'] = RepeatLog::whereUser_id(Auth::user()->id)->sum('amount');
        $data['withdraw'] = WithdrawLog::whereUser_id(Auth::user()->id)->whereIn('status',[2])->sum('amount');
        $data['refer'] = User::where('under_reference',Auth::user()->id)->count();
        return view('user.dashboard',$data);
    }

    public function changePassword()
    {
        $data['page_title'] = "Change Password";
        return view('user.change-password', $data);
    }

    public function submitPassword(Request $request)
    {
        $this->validate($request, [
            'current_password' =>'required',
            'password' => 'required|min:5|confirmed'
        ]);
        try {
            $c_password = Auth::user()->password;
            $c_id = Auth::user()->id;
            $user = User::findOrFail($c_id);
            if(Hash::check($request->current_password, $c_password)){

                $password = Hash::make($request->password);
                $user->password = $password;
                $user->save();
                session()->flash('message', 'Password Changes Successfully.');
                session()->flash('title','Success');
                Session::flash('type', 'success');
                return redirect()->back();
            }else{
                session()->flash('alert', 'Current Password Not Match');
                Session::flash('type', 'warning');
                session()->flash('title','Opps');
                return redirect()->back();
            }

        } catch (\PDOException $e) {
            session()->flash('alert', $e->getMessage());
            Session::flash('type', 'warning');
            session()->flash('title','Opps');
            return redirect()->back();
        }
    }

    public function editProfile()
    {
        $data['page_title'] = "Edit Profile";
        $data['user'] = User::findOrFail(Auth::user()->id);
        return view('user.edit-profile', $data);
    }

    public function submitProfile(Request $request)
    {
        $user = User::findOrFail(Auth::user()->id);
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,
            'phone' => 'required|string|min:10|unique:users,phone,'.$user->id,
            'username' => 'required|min:5|unique:users,username,'.$user->id,
            'image' => 'mimes:png,jpg,jpeg'
        ]);
        $in = Input::except('_method','_token');
        $in['reference'] = $request->username;
        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename = $request->username.'.'.$image->getClientOriginalExtension();
            $location = 'assets/images/' . $filename;
            $in['image'] = $filename;
            if ($user->image != 'user-default.png'){
                $path = './assets/images/';
                $link = $path.$user->image;
                if (file_exists($link)) {
                    unlink($link);
                }
            }
            Image::make($image)->resize(400,400)->save($location);
        }
        $user->fill($in)->save();
        session()->flash('message', 'Profile Updated Successfully.');
        session()->flash('title','Success');
        Session::flash('type', 'success');
        return redirect()->back();
    }

    public function submitVerify(Request $request)
    {
        $user = User::findOrFail(Auth::user()->id);
        $request->validate([
            'no_ktp' => 'required|string|max:255',
            'place_birth' => 'required|string',
            'date_birth' => 'required|string',
            'sex' => 'required|string',
            'nationality' => 'required|string',
            'ktp_image' => 'mimes:png,jpg,jpeg|max:3000',
            'verify_photo' => 'mimes:png,jpg,jpeg|max:3000'
        ]);
        $in = Input::except('_method','_token');
        $ktpName = "";
        $verifyName = "";
        if($request->hasFile('ktp_image') && $request->hasFile('verify_photo')){
            $ktp = $request->file('ktp_image');
            $ktpName = $request->name.'.'.$ktp->getClientOriginalExtension();
            $ktpLocation = 'assets/images/ktp/' . $ktpName;
            Image::make($ktp)->resize(400,400)->save($ktpLocation);
        }

        if ( $request->hasFile('verify_photo')) {
            $verify = $request->file('verify_photo');
            $verifyName = $request->name.'.'.$verify->getClientOriginalExtension();
            $verifyLocation = 'assets/images/verify/' . $verifyName;
            Image::make($verify)->resize(400,400)->save($verifyLocation);
        }

        $in['ktp_image'] = $ktpName;
        $in['verify_photo'] = $verifyName;
        $user->fill($in)->save();
        session()->flash('message', 'Profile Updated Successfully.');
        session()->flash('title','Success');
        Session::flash('type', 'success');
        return redirect()->back();
    }

    public function depositMethod()
    {
        $data['page_title'] = 'Deposit Method';
        $data['paypal'] = PaymentMethod::whereId(1)->first();
        $data['perfect'] = PaymentMethod::whereId(2)->first();
        $data['btc'] = PaymentMethod::whereId(3)->first();
        $data['stripe'] = PaymentMethod::whereId(4)->first();
//        $count = PaymentMethod::count();
//        $skip = 4;
//        $limit = $count - $skip;
//        $data['bank'] = PaymentMethod::orderBy('id','asc')->skip($skip)->take($limit)->whereStatus(1)->get();
        $data['bank'] = PaymentMethod::orderBy('id','asc')->where('id', '>', 4)->whereStatus(1)->get();
        return view('user.deposit-fund',$data);
    }
    public function submitDepositFund(Request $request)
    {
        $basic = BasicSetting::first();
        $this->validate($request,[
            'amount' => 'required',
            'payment_type' => 'required',
        ]);
        $pay_id = $request->payment_type;
        $amou = $request->amount;
        if ($pay_id == 1) {
            $paypal = PaymentMethod::whereId(1)->first();
            $charge = ($paypal->fix + ( $amou*$paypal->percent / 100 ));
        }elseif ($pay_id == 2) {
            $paypal = PaymentMethod::whereId(2)->first();
            $charge = ($paypal->fix + ( $amou*$paypal->percent / 100 ));
        }elseif ($pay_id == 3) {
            $paypal = PaymentMethod::whereId(3)->first();
            $charge = ($paypal->fix + ( $amou*$paypal->percent / 100 ));
        }elseif ($pay_id == 4) {
            $paypal = PaymentMethod::whereId(4)->first();
            $charge = ($paypal->fix + ( $amou*$paypal->percent / 100 ));
        }else{
            $paypal = PaymentMethod::whereId($pay_id)->first();
            $charge = $paypal->fix + ( $amou*$paypal->percent / 100 );
        }
        $lo['member_id'] = Auth::user()->id;
        $lo['custom'] = strtoupper(Str::random(20));
        $lo['amount'] = $amou;
        $lo['charge'] = round($charge,$basic->deci);
        $lo['net_amount'] = $amou + $charge;
        $lo['usd'] = round(($amou + $charge) / $paypal->rate,2);
        $lo['payment_type'] = $request->payment_type;
        $data['fund'] = PaymentLog::create($lo);
        return redirect()->route('deposit-preview',$data['fund']->custom);
    }
    public function depositPreview($id)
    {
        if (url()->previous() == url('/')."/user/deposit-fund" || url()->previous() != url()->current()) {
            $data['fund'] = PaymentLog::whereCustom($id)->first();
            $data['page_title'] = "Deposit Preview";
            $data['paypal'] = PaymentMethod::whereId(1)->first();
            $data['perfect'] = PaymentMethod::whereId(2)->first();
            $data['btc'] = PaymentMethod::whereId(3)->first();
            $data['stripe'] = PaymentMethod::whereId(4)->first();
    //        $count = PaymentMethod::count();
    //        $skip = 4;
    //        $limit = $count - $skip;
    //        $data['bank'] = PaymentMethod::orderBy('id','asc')->skip($skip)->take($limit)->whereStatus(1)->get();
            $data['bank'] = PaymentMethod::orderBy('id','asc')->where('id', '>', '4')->whereStatus(1)->get();
            return view('user.deposit-preview',$data);
        }else{
            abort(404);
        }
    }
    public function manualDepositSubmit(Request $request)
    {
        $this->validate($request,[
            'image' => 'required',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'fund_id' => 'required'
        ]);
        $fund = PaymentLog::findOrFail($request->fund_id);

        $de['user_id'] = Auth::user()->id;
        $de['amount'] = $fund->amount;
        $de['charge'] = $fund->charge;
        $de['net_amount'] = $fund->net_amount;
        $de['payment_type'] = $fund->payment_type;
        $de['message'] = $request->message;
        $de['transaction_id'] = $fund->custom;
        $dep = Deposit::create($de);

        if($request->hasFile('image')){
            $image3 = $request->file('image');
            foreach ($image3 as $img){
                $filename3 = time().'h3'.'.'.$img->getClientOriginalExtension();
                $location = 'assets/deposit/' . $filename3;
                Image::make($img)->save($location);
                $in['image'] = $filename3;
                $in['deposit_id'] = $dep->id;
                DepositImage::create($in);
            }
        }
        session()->flash('message', 'Deposit Successfully Submitted. Wait For Confirmation.');
        Session::flash('type', 'success');
        Session::flash('title', 'Completed');
        return redirect()->route('deposit-fund');

    }
    public function historyDepositFund()
    {
        $data['page_title'] = "Deposit History";
        $data['deposit'] = Deposit::whereUser_id(Auth::user()->id)->orderBy('id','desc')->get();
        return view('user.deposit-history',$data);
    }
    public function userActivity()
    {
        $data['page_title'] = "Transaction Log";
        $data['log'] = UserLog::whereUser_id(Auth::user()->id)->orderBy('id','desc')->paginate(15);
        return view('user.user-activity',$data);
    }
    public function withdrawRequest()
    {

        $data['page_title'] = "Withdraw Method";
        $data['basic'] = BasicSetting::first();
        if ($data['basic']->withdraw_status == 0){
            session()->flash('message','Currently Withdraw Is Deactivated.');
            session()->flash('type','warning');
            session()->flash('title','Warning');
        }
        $data['method'] = WithdrawMethod::whereStatus(1)->get();
        return view('user.withdraw-request',$data);
    }
    public function submitWithdrawRequest(Request $request)
    {
        $this->validate($request,[
            'method_id' => 'required',
            'amount' => 'required'
        ]);
        $basic = BasicSetting::first();
        $bal = User::findOrFail(Auth::user()->id);
        $method = WithdrawMethod::findOrFail($request->method_id);
        $ch = $method->fix + round(($request->amount * $method->percent) / 100,$basic->deci);
        $reAmo = $request->amount + $ch;
        if ($reAmo < $method->withdraw_min){
            session()->flash('message','Your Request Amount is Smaller Then Withdraw Minimum Amount.');
            session()->flash('type','warning');
            session()->flash('title','Opps');
            return redirect()->back();
        }
        if ($reAmo > $method->withdraw_max){
            session()->flash('message','Your Request Amount is Larger Then Withdraw Maximum Amount.');
            session()->flash('type','warning');
            session()->flash('title','Opps');
            return redirect()->back();
        }
        if ($reAmo > $bal->balance){
            session()->flash('message','Your Request Amount is Larger Then Your Current Balance.');
            session()->flash('type','warning');
            session()->flash('title','Opps');
            return redirect()->back();
        }else{
            $tr = strtoupper(Str::random(20));
            $w['amount'] = $request->amount;
            $w['method_id'] = $request->method_id;
            $w['charge'] = $ch;
            $w['transaction_id'] = $tr;
            $w['net_amount'] = $reAmo;
            $w['user_id'] = Auth::user()->id;
            $trr = WithdrawLog::create($w);
            return redirect()->route('withdraw-preview',$trr->transaction_id);
        }
    }
    public function previewWithdraw($id)
    {
        $data['page_title'] = "Withdraw Method";
        $data['withdraw'] = WithdrawLog::whereTransaction_id($id)->first();
        $data['method'] = WithdrawMethod::findOrFail($data['withdraw']->method_id);
        $data['balance'] = User::findOrFail(Auth::user()->id);
        return view('user.withdraw-preview',$data);
    }
    public function submitWithdraw(Request $request)
    {
        $basic = BasicSetting::first();
        $this->validate($request,[
            'withdraw_id' => 'required',
            'send_details' => 'required'
        ]);
        $ww = WithdrawLog::findOrFail($request->withdraw_id);
        $ww->send_details = $request->send_details;
        $ww->message = $request->message;
        $ww->status = 1;
        $ww->save();

        $bal4 = User::findOrFail(Auth::user()->id);
        $ul['user_id'] = $bal4->id;
        $ul['amount'] = $ww->amount;
        $ul['charge'] = $ww->charge;
        $ul['amount_type'] = 5;
        $ul['post_bal'] = $bal4->balance - $ww->amount;
        $ul['description'] = $ww->amount." ".$basic->currency." Withdraw Request Send. Via ".$ww->method->name;
        $ul['transaction_id'] = $ww->transaction_id;
        UserLog::create($ul);

        $bal4 = User::findOrFail(Auth::user()->id);
        $ul['user_id'] = $bal4->id;
        $ul['amount'] = $ww->charge;
        $ul['charge'] = null;
        $ul['amount_type'] = 10;
        $ul['post_bal'] = $bal4->balance - ($ww->amount + $ww->charge);
        $ul['description'] = $ww->charge." ".$basic->currency." Charged for Withdraw Request. Via ".$ww->method->name;
        $ul['transaction_id'] = $ww->transaction_id;
        UserLog::create($ul);

        $bal4->balance = $bal4->balance - $ww->net_amount;
        $bal4->save();

        if ($basic->email_notify == 1){
            $text = $ww->amount." - ". $basic->currency." Withdraw Request Send via ".$ww->method->name.". <br> Transaction ID Is : <b>#$ww->transaction_id</b>";
            $this->sendMail($bal4->email,$bal4->name,'Withdraw Request.',$text);
        }
        if ($basic->phone_notify == 1){
            $text = $ww->amount." - ". $basic->currency." Withdraw Request Send via ".$ww->method->name.". <br> Transaction ID Is : <b>#$ww->transaction_id</b>";
            $this->sendSms($bal4->phone,$text);
        }

        session()->flash('message','Withdraw request Successfully Submitted. Wait For Confirmation.');
        session()->flash('type','success');
        session()->flash('title','Success');
        return redirect()->route('withdraw-log');

    }
    public function withdrawLog()
    {
        $data['page_title'] = "Withdraw Log";
        $data['log'] = WithdrawLog::whereUser_id(Auth::user()->id)->whereNotIn('status',[0])->orderBy('id','desc')->get();
        return view('user.withdraw-log',$data);
    }
    public function openSupport()
    {
        $data['page_title'] = "Open Support Ticket";
        return view('user.support-open', $data);
    }
    public function submitSupport(Request $request)
    {
        $this->validate($request,[
            'subject' => 'required',
            'message' => 'required'
        ]);
        $s['ticket_number'] = strtoupper(Str::random(12));
        $s['user_id'] = Auth::user()->id;
        $s['subject'] = $request->subject;
        $s['status'] = 1;
        $mm = Support::create($s);
        $mess['support_id'] = $mm->id;
        $mess['ticket_number'] = $mm->ticket_number;
        $mess['message'] = $request->message;
        $mess['type'] = 1;
        SupportMessage::create($mess);
        session()->flash('success','Support Ticket Successfully Open.');
        session()->flash('type','success');
        session()->flash('title','Success');
        return redirect()->route('support-all');
    }
    public function allSupport()
    {
        $data['page_title'] = "All Support Ticket";
        $data['support'] = Support::whereUser_id(Auth::user()->id)->orderBy('id','desc')->get();
        return view('user.support-all',$data);
    }
    public function supportMessage($id)
    {
        $data['page_title'] = "Support Message";
        $data['support'] = Support::whereTicket_number($id)->first();
        $data['message'] = SupportMessage::whereTicket_number($id)->orderBy('id','asc')->get();
        return view('user.support-message', $data);
    }
    public function userSupportMessage(Request $request)
    {
        $this->validate($request,[
            'message' => 'required',
            'support_id' => 'required'
        ]);
        $mm = Support::findOrFail($request->support_id);
        $mm->status = 3;
        $mm->save();
        $mess['support_id'] = $mm->id;
        $mess['ticket_number'] = $mm->ticket_number;
        $mess['message'] = $request->message;
        $mess['type'] = 1;
        SupportMessage::create($mess);
        session()->flash('message','Support Ticket Successfully Reply.');
        session()->flash('type','success');
        session()->flash('title','Success');
        return redirect()->back();
    }
    public function supportClose(Request $request)
    {
        $this->validate($request,[
            'support_id' => 'required'
        ]);
        $su = Support::findOrFail($request->support_id);
        $su->status = 9;
        $su->save();
        session()->flash('message','Support Successfully Closed.');
        session()->flash('type','success');
        session()->flash('title','Success');
        return redirect()->back();
    }

    public function newInvest()
    {
        $data['basic_setting'] = BasicSetting::first();
        $data['page_title'] = "User New Invest";
        $data['plan'] = Plan::whereStatus(1)->get();
        return view('user.investment-new',$data);
    }

    public function postInvest(Request $request)
    {
        $this->validate($request,[
            'id' => 'required'
        ]);
        $data['page_title'] = "Investment Preview";
        $data['plan'] = Plan::findOrFail($request->id);
        return view('user.investment-preview',$data);
    }

    public function chkInvestAmount(Request $request)
    {
        $plan = Plan::findOrFail($request->plan);
        $user = User::findOrFail(Auth::user()->id);
        $amount = $request->amount;

        if ($request->amount > $user->balance){
            return '<div class="col-sm-7 col-sm-offset-4">
                <div class="alert alert-warning"><i class="fa fa-times"></i> Amount Is Larger than Your Current Amount.</div>
            </div>
            <div class="col-sm-7 col-sm-offset-4">
                <button type="button" class="btn btn-warning btn-block bold uppercase btn-lg delete_button disabled"
                        >
                    <i class="fa fa-cloud-upload"></i> Invest Amount Under This Package
                </button>
            </div>';
        }
        if( $plan->minimum > $amount){
            return '<div class="col-sm-7 col-sm-offset-4">
                <div class="alert alert-warning"><i class="fa fa-times"></i> Amount Is Smaller than Plan Minimum Amount.</div>
            </div>
            <div class="col-sm-7 col-sm-offset-4">
                <button type="button" class="btn btn-warning btn-block bold uppercase btn-lg  delete_button disabled"
                        >
                    <i class="fa fa-cloud-upload"></i> Invest Amount Under This Package
                </button>
            </div>';
        }elseif( $plan->maximum < $amount){
            return '<div class="col-sm-7 col-sm-offset-4">
                <div class="alert alert-warning"><i class="fa fa-times"></i> Amount Is Larger than Plan Maximum Amount.</div>
            </div>
            <div class="col-sm-7 col-sm-offset-4">
                <button type="button" class="btn btn-warning btn-block bold uppercase btn-lg delete_button disabled"
                      >
                    <i class="fa fa-cloud-upload"></i> Invest Amount Under This Package
                </button>
            </div>';
        }else{
            return '<div class="col-sm-7 col-sm-offset-4">
                <div class="alert alert-success"><i class="fa fa-check"></i> Well Done. Invest This Amount Under this Package.</div>
            </div>
            <div class="col-sm-7 col-sm-offset-4">
                <button type="button" class="btn btn-warning bold uppercase btn-block btn-lg delete_button"
                        data-toggle="modal" data-target="#DelModal"
                        data-id='.$amount.'>
                    <i class="fa fa-cloud-upload"></i> Invest Amount Under This Package
                </button>
            </div>';
        }

    }

    public function submitInvest(Request $request)
    {
        $basic = BasicSetting::first();
        $request->validate([
           'amount' => 'required',
            'user_id' => 'required',
            'plan_id' => 'required'
        ]);
        $in = Input::except('_method','_token');
        $in['trx_id'] = strtoupper(Str::random(20));
        $invest = Investment::create($in);

        $pak = Plan::findOrFail($request->plan_id);
        $com = Compound::findOrFail($pak->compound_id);
        $rep['user_id'] = $invest->user_id;
        $rep['investment_id'] = $invest->id;
        $rep['repeat_time'] = Carbon::parse()->addHours($com->compound);
        $rep['total_repeat'] = 0;
        Repeat::create($rep);

        $bal4 = User::findOrFail(Auth::user()->id);
        $ul['user_id'] = $bal4->id;
        $ul['amount'] = $request->amount;
        $ul['charge'] = null;
        $ul['amount_type'] = 14;
        $ul['post_bal'] = $bal4->balance - $request->amount;
        $ul['description'] = $request->amount." ".$basic->currency." Invest Under ".$pak->name." Plan.";
        $ul['transaction_id'] = $in['trx_id'];
        UserLog::create($ul);

        $bal4->balance = $bal4->balance - $request->amount;
        $bal4->save();

        $trx = $in['trx_id'];

        if ($basic->email_notify == 1){
            $text = $request->amount." - ". $basic->currency." Invest Under ".$pak->name." Plan. <br> Transaction ID Is : <b>#$trx</b>";
            $this->sendMail($bal4->email,$bal4->name,'New Investment',$text);
        }
        if ($basic->phone_notify == 1){
            $text = $request->amount." - ". $basic->currency." Invest Under ".$pak->name." Plan. <br> Transaction ID Is : <b>#$trx</b>";
            $this->sendSms($bal4->phone,$text);
        }

        session()->flash('success','Investment Successfully Completed.');
        session()->flash('type','success');
        session()->flash('title','Success');
        return redirect()->back();
    }

    public function historyInvestment()
    {
        $data['page_title'] = "Invest History";
        $data['history'] = Investment::whereUser_id(Auth::user()->id)->orderBy('id','desc')->get();
        return view('user.investment-history',$data);
    }


    public function spotMarket()
    {
        $data['page_title'] = 'Spot Market';
        return view('user.spot-market', $data);
    }

    public function verification()
    {
        $data['page_title'] = 'Account Verification';
        return view('user.verify',$data);
    }

    public function repeatLog()
    {
        $data['user'] = User::findOrFail(Auth::user()->id);
        $data['page_title'] = 'All Repeat';
        $data['log'] = RepeatLog::whereUser_id(Auth::user()->id)->orderBy('id','desc')->paginate(15);
        return view('user.repeat-history',$data);
    }

    public function detailMarket($id){
        $data['page_title'] = $id;
        $data['market'] = (substr($id, 3,6) == 'IDR') ? substr($id, 0,3).'/'.substr($id, 3,6) : '';
        $data['real_id'] = strtolower($id);
        $data['from'] = substr($id, 0,3);
        $data['to'] = substr($id, 3,6);
        $data['title_market']  = $this->find(strtolower(substr($id, 0,3)))['name'];
        return view('user.detailMarket', $data);
    }  

    public function find($id){
        $result = null;
        if($id == "acn"){
            $acn = new BitcoinClient('http://activacoinrpc:E6pJ5afCo4jc9HjkEeGz9HgzBki1eEFjm8L5KgLmjoqv@167.99.6.124:2332/');
            $address = json_decode($acn->getAccountAddress(Auth::user()->email)->getBody())->result;
            $balance = json_decode($acn->getBalance(Auth::user()->email)->getBody())->result;
            // $address = "";
            // $balance = 0;
            $result = array('address' => $address, 'name' => 'Activa Coin', 'balance' => $balance, "name" => 'ACTIVA COIN');
        }else if ($id=="btc") {
            $address = "";
            $balance = 0;
            $result = array('address' => $address, 'name' => 'Bitcoin', 'balance' => $balance, "name" => 'BITCOIN');
        }else if($id=="bch"){
            $balance = 0;
            $address = "";
            $result = array('address' => $address, 'name' => 'Bitcoin Cash', 'balance' => $balance, "name" => 'BITCOIN CASH');
        }else if($id=="btg"){
            $address = "";
            $balance = 0;
            $result = array('address' => $address, 'name' => 'Bitcoin Gold', 'balance' => $balance, 'name' => 'BITCOIN GOLD');
        }
        return $result;
    }

    public function finance()
    {
        $data['page_title'] = 'Finance';
        $data['balance_acn'] = $this->find("acn")['balance'];
        return view('user.finance', $data);
    }

    public function financeDetail($id)
    {
        $data['page_title'] = strtoupper($id);
        $data['address'] = $this->find($id)['address'];
        $data['balance'] = $this->find($id)['balance'];
        $data['market'] = $this->find($id)['name'];
        return view('user.send-receive', $data);
    }

    public function buyACN(Request $request)
    {
        $client = new Client([
            'base_uri' => 'https://activacoin.com',
        ]);
        $res = $client->request('GET', '/api/price/getPrice');
        $idrNow = json_decode($res->getBody())->response->idr;
        $acn = $request->rp / $idrNow;
        $this->transferAcn();
    }

    public function transferAcn($value='')
    {
    	$acn = new BitcoinClient('http://activacoinrpc:E6pJ5afCo4jc9HjkEeGz9HgzBki1eEFjm8L5KgLmjoqv@167.99.6.124:2332/');
    }

    public function marketBuy(Request $request){
        DB::insert('insert into trans_buy_sell (user_id,type,pair,price,created_at,updated_at) values (?, ?, ?, ?, ?, ?)', [Auth::user()->id, 'buy', $request->pair, $request->buy_amount_rp, $a = date('Y-m-d H:i:s'), date('Y-m-d H:i:s')]);
        User::whereId(Auth::user()->id)->update(['balance' => Auth::user()->balance - $request->buy_amount_rp]);
        return response()->json(['total_trade_rp' => $request->buy_amount_rp, 'total_trade_btc' => $request->btc , 'total_fee_rp' => $request->fee, 'time' => $a]);
    }

//    public function userReference()
//    {
//        $data['page_title'] = "Reference User";
//        $data['user'] = User::whereUnder_reference(Auth::user()->id)->orderBy('id','desc')->get();
//        return view('user.reference-user',$data);
//    }

}
