<?php

namespace App\Http\Controllers;

use Google2FA;
use Auth;
use Storage;
use Illuminate\Http\Request;
use App\User;
use Session;

class Google2FAController extends Controller
{
    public function index()
    {        
    	$key = Google2FA::generateSecretKey();

        $user = User::find(Auth::user()->id);
        $user->google2fa_secret = $key;
        $user->save();

        $googleUrl = $this->getGoogleUrl($key);
        $page_title = "Google Authenticator";
        return view('user.2fa.enableTwoFactor')->with(compact('key', 'googleUrl', 'page_title'));
    }


    public function check2fa()
    {
    	if (! $code = request()->get('google2fa_secret'))
        {
            return false;
        }
        // Verify the code
        $valid =  Google2FA::verifyKey(Auth::user()->google2fa_secret, $code);
        if ($valid === false) {
        	return redirect()->back()->withErrors(['message' => 'INVALID']);
        }
        Session::flash('message', 'Terimakasih sudah mengaktifkan Google Authenticator'); 
        return redirect()->route('user-dashboard');
    }

    private function getGoogleUrl($key)
    {
        return Google2FA::getQRCodeGoogleUrl(
            Auth::user()->name,
            Auth::user()->email,
            $key
        );
    }
}
