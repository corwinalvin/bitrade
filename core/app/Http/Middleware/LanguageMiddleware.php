<?php
namespace App\Http\Middleware;
use Request, App, View;
use Closure;
use Illuminate\Foundation\Application;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class LanguageMiddleware
{
    public function handle($request, Closure $next)
    {
        if (Session::has('active_lang') AND array_key_exists(Session::get('active_lang'), Config::get('languages'))) {
            App::setLocale(Session::get('active_lang'));
        }
        else { // This is optional as Laravel will automatically set the fallback language if there is none specified
            App::setLocale(Config::get('app.fallback_locale'));
        }
        return $next($request);
    }
}